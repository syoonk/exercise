// SPDX-License-Identifier: MIT
pragma solidity =0.7.0;

// Import OpenZeppelin Contract
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

// This ERC-20 contract mints the specified amount of tokens to the contract creator.
contract MyToken is ERC20 {
    constructor(uint256 initialSupply) ERC20("Kong Token", "KNG") 
    {
        _mint(msg.sender, initialSupply);
    }

    function mint(address _to, uint amount)
    external {
        _mint(_to, amount);
    }
}
