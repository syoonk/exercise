import { MobxProvider } from "./hooks/useMobxStore.js";
import WalletConnector from "./actions/WalletConnector";
import Token from "./actions/Token";

import transakSDK from "@transak/transak-sdk";

const settings = {
    apiKey: 'cf5868eb-a8bb-45c8-a2db-4309e5f8b412',  // Your API Key
    environment: 'STAGING', // STAGING/PRODUCTION
    defaultCryptoCurrency: 'ETH',
    themeColor: '000000', // App theme color
    hostURL: window.location.origin,
    widgetHeight: "700px",
    widgetWidth: "500px",
}

export function openTransak() {
    const transak = new transakSDK(settings);

        transak.init();
        // To get all the events
        transak.on(transak.ALL_EVENTS, (data) => {
            //console.log(data)
        });
        // This will trigger when the user marks payment is made.
        transak.on(transak.EVENTS.TRANSAK_ORDER_SUCCESSFUL, (orderData) => {
          //console.log(orderData);
          //transak.close();
          console.log("AFTER DONE:: ");
        });
}

function App() {
  return (
    <div style={{paddingBottom: "50px"}}>
      <MobxProvider>
        <button btn btn-primary onClick={e => openTransak(e)}>Open Transak</button>
        <WalletConnector></WalletConnector>
        <Token></Token>
      </MobxProvider>
    </div>
  );
}

export default App;
