import React, { createContext, useEffect } from "react";
import { useLocalObservable } from "mobx-react";
import { runInAction } from "mobx";
import Web3 from 'web3';

import { Endpoints, web3Modal } from "../constants/walletEnvs.js";

export const ProviderContext = createContext();

export const Web3Provider = (props) => {
  const providerStore = useLocalObservable(() => ({
    address: "",
    provider: null,
    connected: false,
    chainId: 1,
    networkId: 1,
    web3: new Web3(Endpoints[1]),

    initWeb3(provider) {
      const web3 = new Web3(provider);
    
      web3.eth.extend({
        methods: [
          {
            name: "chainId",
            call: "eth_chainId",
            outputFormatter: web3.utils.hexToNumber
          }
        ]
      });
    
      return web3;
    },

    async subscribeProvider(provider) {
      if (!provider.on) {
        return;
      }
  
      if(window.ethereum) {
          window.ethereum.autoRefreshOnNetworkChange = false;
          window.ethereum.on('accountsChanged', (accounts) => {
            // Handle the new accounts, or lack thereof.
            // "accounts" will always be an array, but it can be empty.
            runInAction(() => {
              this.address = accounts[0];
            });
          });
          
          window.ethereum.on('chainChanged', (_chainId) => {
            // Handle the new chain.
            // Correctly handling chain changes can be complicated.
            // We recommend reloading the page unless you have a very good reason not to.
            // window.location.reload();
            const chainId = Web3.utils.hexToNumber(_chainId);
            runInAction(() => {
              this.chainId = chainId;
              this.networkId = chainId;
            });
          });
        } else {
          console.log("ERROR:: CONNECT_WALLET:: INVALID_WALLET");
        }
    },
  
    async onConnect() {
      const provider = await web3Modal.connect();
      const web3 = new this.initWeb3(provider);
      const accounts = await web3.eth.getAccounts();
      const address = accounts[0];
      const networkId = await web3.eth.net.getId();
      const chainId = await web3.eth.chainId();

      runInAction(() => {
        this.provider = provider;
        this.address = address;
        this.connected = true;
        this.chainId = chainId;
        this.networkId = networkId;
        this.web3 = web3;
      });

      await this.subscribeProvider(provider);
    },

    async resetApp() {
      if (this.web3 && this.web3.currentProvider && this.web3.currentProvider.close) {
        await this.web3.currentProvider.close();
      }
      web3Modal.clearCachedProvider();
      
      this.address= "";
      this.provider= null;
      this.connected = false;
      this.web3 = new Web3(Endpoints[this.chainId]);
    }
  }));

  useEffect(() => {
    if(web3Modal && web3Modal.cachedProvider) { providerStore.onConnect(); }
  }, );

  return <ProviderContext.Provider value={providerStore} {...props} />
}  
