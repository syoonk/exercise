import TokenJson from "../abis/MyToken.json";

export const getTokenContract = (provider, address) => {
  if(!provider.web3) {
    return alert("Invalid provider");
  }

  return new provider.web3.eth.Contract(TokenJson.abi, address);
}