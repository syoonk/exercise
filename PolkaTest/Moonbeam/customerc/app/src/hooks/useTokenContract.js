import { useMemo } from "react";

import { getTokenContract } from "../utils/getContractInsts";

export const useTokenContract = (provider, address) => {
  return useMemo(() => {
    if(!provider.chainId || !provider.web3) {
      return ;
    }

    return getTokenContract(provider, address);
  }, [provider, address]);
}
