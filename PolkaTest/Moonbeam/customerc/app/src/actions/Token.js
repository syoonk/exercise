import { useState } from "react";
import { observer } from "mobx-react";

import { useMobxStore } from "../hooks/useMobxStore.js";
import { getTokenContract } from "../utils/getContractInsts";

const Home = observer(() => {
  const { ProviderStore } = useMobxStore();

  const [tokenAddress, setTokenAddress] = useState("");

  const [tokenContract, setTokenContract] = useState(undefined);

  const setContract = (e) => {
    e.preventDefault();

    setTokenContract(getTokenContract({web3: ProviderStore.web3}, tokenAddress));
  }

  const [mintTo, setMintTo] = useState("");
  const [mintAmount, setMintAmount] = useState("");
  const mint = async (e) => {
    e.preventDefault();

    if(!tokenContract) {
      alert("no contract object set");
      return ;
    }

    if(!mintTo || !mintAmount) {
      alert("value is empty");
      return ;
    }

    const receipt = await tokenContract.methods
      .mint(mintTo, mintAmount)
      .send({from: ProviderStore.address});
    
    console.log("RECEIPT:: \n", receipt);
  }

  const [transferTo, setTransferTo] = useState("");
  const [transferAmount, setTransferAmount] = useState("");
  const transfer = async (e) => {
    e.preventDefault();
    
    if(!tokenContract) {
      alert("no contract object set");
      return ;
    }

    if(!transferTo || !transferAmount) {
      alert("value is empty");
      return ;
    }

    const receipt = await tokenContract.methods
      .transfer(transferTo, transferAmount)
      .send({from: ProviderStore.address});

    console.log("RECEIPT:: \n", receipt);
  }

  const [balanceUser, setBalanceUser] = useState("");
  const getBalance = async (e) => {
    e.preventDefault();

    if(!tokenContract) {
      alert("no contract object set");
      return ;
    }

    if(!balanceUser) {
      alert("value is empty");
      return ;
    }

    const result = await tokenContract.methods.balanceOf(balanceUser).call();
    
    alert(`${balanceUser} has balance: ${result}`);
    console.log(`${balanceUser} has balance: ${result}`);
  }

  return (
    <div className="container">
      <h1>Moonbase Alpha V6</h1>
      <h2>Token ({!tokenContract ? "Contract is not set yet" : `Contract at ${tokenContract.options.address}`})</h2>
      <form>
        <legend>Set Token Contract</legend>
        <input
          className="form-control" 
          placeholder="Token Address here"
          onChange={(e) => setTokenAddress(e.target.value)}
        ></input>
        <button
          className="btn btn-primary"
          onClick={(e) => setContract(e)}
        >Submit</button>
      </form>
      <div>
        <form>
          <legend>Mint</legend>
          <label>To</label>
          <input
            className="form-control"
            placeholder="Address here"
            defaultValue=""
            onChange={e => setMintTo(e.target.value)}
          ></input>
          <label>Amount</label>
          <input
            className="form-control"
            placeholder="integer"
            defaultValue=""
            onChange={e => setMintAmount(e.target.value)}
          ></input>
          <button
            className="btn btn-primary"
            onClick={e => mint(e)}
          >Submit</button>
        </form>
      </div>
      <div>
        <form>
          <legend>Token Transfer</legend>
          <label>To</label>
          <input 
            className="form-control" 
            placeholder="Address here"
            defaultValue=""
            onChange={(e) => setTransferTo(e.target.value)}
          ></input>
          <label>Amount</label>
          <input 
            className="form-control" 
            placeholder="integer"
            defaultValue=""
            onChange={(e) => setTransferAmount(e.target.value)}
          ></input>
          <button
            className="btn btn-primary"
            onClick={(e) => transfer(e)}
          >Submit</button>
        </form>
      </div>
      <div>
        <form>
          <legend>Get Balance</legend>
          <label>Address</label>
          <input 
            className="form-control" 
            placeholder="User Address here"
            defaultValue=""
            onChange={(e) => setBalanceUser(e.target.value)}
          ></input>
          <button
            className="btn btn-primary"
            onClick={(e) => getBalance(e)}
          >Submit</button>
        </form>
      </div>
    </div>
  )
})

export default Home;
