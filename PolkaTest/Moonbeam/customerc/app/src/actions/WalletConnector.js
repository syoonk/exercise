import { observer } from "mobx-react";
import { toJS } from "mobx";

import { useMobxStore } from "../hooks/useMobxStore.js";

const WalletConnector = observer(() => {
  const { ProviderStore } = useMobxStore();

  const printProvider = () => {
    console.log("Provider:: ", toJS(ProviderStore));
  }
  
  return (
    <div>
    { !ProviderStore.connected ? (
      <div>
        <button
          className="btn btn-primary"
          onClick={ProviderStore.onConnect}
        >
        Connect Wallet
        </button>
        <h1>Connect Wallet!!! Or You will get error</h1>
      </div>
    ) : ( 
      <div>
        <button className="btn btn-primary btn-sm" onClick={printProvider}>print provider</button>
        <button className="btn btn-warning btn-sm" onClick={ProviderStore.resetApp}>Disconnect</button>
        <p>Current ChainId: {ProviderStore.chainId}</p>
        <p>Current Account: {ProviderStore.address}</p>
      </div> 
    )}
    </div>
  )
});

export default WalletConnector;
