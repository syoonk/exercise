const PrivateKeyProvider = require('./private-provider');

const privateKeyDev =
  'f7c9a8bd547b9255eca7648e81ba3b2046ab40550f74de95344a1133fc1a4684';

module.exports = {
  networks: {
    alphamoon: {
      provider: () => {
        return new PrivateKeyProvider(privateKeyDev, 'https://rpc.testnet.moonbeam.network', 1287);
      },
      network_id: 1287,
      gas: 5100000
    }
  },

  // Configure your compilers
  compilers: {
    solc: {
      version: "0.7.0",    // Fetch exact version from solc-bin (default: truffle's version)
    }
  },

  plugins: ['moonbeam-truffle-plugin']
}
