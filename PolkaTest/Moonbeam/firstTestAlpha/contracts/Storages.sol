pragma solidity ^0.7.0;

contract Storages {
    
    string private privateStr;
    string public publicStr;
    
    constructor() {
        privateStr = "nodata";
        publicStr = "nodata";
    }
    
    function setPrivateStr(string calldata _str)
    external {
        privateStr = _str;
    }
    
    function setPublicStr(string calldata _str)
    external {
        publicStr = _str;
    }
    
    function getPrivateStr()
    external view
    returns(string memory) {
        return privateStr;
    }
    
    function getPublicStr()
    external view
    returns(string memory) {
        return publicStr;
    }
        
}