class Node {

    constructor(value = "") {
        this.value = value;
        this.end = false;
        this.child = {};
    }

}

class Trie {

    constructor() {
        this.root = new Node();
    }

    insert(_str) {
        let currentNode = this.root;

        for(let i = 0; i < _str.length; i++) {
            const currentChar = _str[i];

            if(currentNode.child[currentChar] === undefined) {
                currentNode.child[currentChar] = new Node(currentNode.value + currentChar);
            }

            currentNode = currentNode.child[currentChar];
        }

        currentNode.end = true;
    }

    search(_str) {
        let currentNode = this.root;

        for(let i = 0; i < _str.length; i++) {
            const currentChar = _str[i];

            if(currentNode.child[currentChar]) {
                currentNode = currentNode.child[currentChar];
            } else {
                return "";
            }
        }

        return currentNode.value;
    }

}

const myTrie = new Trie();

myTrie.insert("hello");
myTrie.insert("help");
myTrie.insert("carry");

console.log(myTrie);
console.log(myTrie.root.child.h.child.e);

console.log(myTrie.search("hel"));
console.log(myTrie.search("held"));