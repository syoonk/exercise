/*

    Given an array, A , of N integers, print A's elements in reverse order 
    as a single line of space-separated numbers.

    i.e.
    input : 1 4 3 2 -> 2 3 4 1

*/

function reverseArray(n, arr) {
    arr.reverse();

    var printString = '';

    arr.forEach(element => (printString += element + ' '))

    console.log(printString);
}