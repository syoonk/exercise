/*

    

*/

class Person {
    constructor(firstName, lastName, identification) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = identification;
    }

    printPerson() {
        console.log(
            "Name: " + this.lastName + ", " + this.firstName
            + "\nID: " + this.idNumber
        )
    }
}


class Student extends Person {
    /*	
    *   Class Constructor
    *   
    *   @param firstName - A string denoting the Person's first name.
    *   @param lastName - A string denoting the Person's last name.
    *   @param id - An integer denoting the Person's ID number.
    *   @param scores - An array of integers denoting the Person's test scores.
    */
    // Write your constructor here
    constructor(_firstName, _lastName, _id, _testScores) {
        super(_firstName, _lastName, _id);
        this.testScores = _testScores;
    }

    calculate() {
        var average = this.testScores.reduce((a, b) => (a + b)) / this.testScores.length;
        // OEAPDT
        switch (true) {
            case (average >= 90): return 'O';
            case (average < 90 && average >= 80): return 'E';
            case (average < 80 && average >= 70): return 'A';
            case (average < 70 && average >= 55): return 'P';
            case (average < 55 && average >= 40): return 'D';
            default: return 'T';
        }
    }
    /*	
    *   Method Name: calculate
    *   @return A character denoting the grade.
    */
    // Write your method here
}