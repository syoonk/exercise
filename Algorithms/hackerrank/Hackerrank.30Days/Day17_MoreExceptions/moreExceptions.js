function Calculator() {
    this.power = (_n, _p) => {
        if (_n >= 0 && _p >= 0) {
            return Math.pow(_n, _p)
        } else {
            throw (`n and p should be non-negative`);
        }
    };
}

function main() {
    var myCalculator = new Calculator();
    var T = parseInt(readLine());
    while (T-- > 0) {
        var num = (readLine().split(" "));
        try {
            var n = parseInt(num[0]);
            var p = parseInt(num[1]);
            var ans = myCalculator.power(n, p);
            console.log(ans);
        }
        catch (e) {
            console.log(e);
        }

    }
}