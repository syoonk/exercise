function main() {
    var n = parseInt(readLine());
    a = readLine().split(' ');
    a = a.map(Number);
    // Write Your Code Here
    let totalSwap = 0;

    for (let s = 0; s < a.length; s++) {
        let loopSwap = 0;

        for (let i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) {
                let dummy = a[i + 1];
                a[i + 1] = a[i];
                a[i] = dummy;
                totalSwap++;
                loopSwap++;
            }
        }
        if(loopSwap == 0) break;
    }

    console.log(`Array is sorted in ${totalSwap} swaps.`);
    console.log(`First Element: ${a[0]}`);
    console.log(`Last Element: ${a[a.length - 1]}`);

}