fn gridChallenge(grid: &[String]) -> String {
  let n: usize = grid.len();

  let mut current_row: Vec<u8> = grid[0].chars().map(|c| c as u8).collect::<Vec<u8>>();
  current_row.sort();
  for i in 0..(n - 1) {
    let mut next_row: Vec<u8> = grid[i + 1].chars().map(|c| c as u8).collect::<Vec<u8>>();
    next_row.sort();

    for j in 0..current_row.len() {
      if current_row[j] > next_row[j] {
        return "NO".to_owned();
      }
    }

    current_row = next_row;
  }

  return String::from("YES");
}