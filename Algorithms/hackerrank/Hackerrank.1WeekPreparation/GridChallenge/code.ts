function gridChallenge(grid: string[]): string {
  let currentGrid = grid[0].split("").sort();
  for(let i = 0; i < grid.length - 1; i++) {
    const nextGrid = grid[i + 1].split("").sort();

    for(let j = 0; j < currentGrid.length; j++) {
      if(currentGrid[j].charCodeAt(0) > nextGrid[j].charCodeAt(0)) {
        return "NO";
      }
    }

    currentGrid = nextGrid;
  }

  return "YES";
}