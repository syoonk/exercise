function countingSort(arr: number[]): number[] {
  const maxNum: number = 100;
  const result: number[] = Array.from({ length: maxNum }, () => 0);

  for(let _num of arr) {
    result[_num] += 1;
  }

  return result;
}