fn countingSort(arr: &[i32]) -> Vec<i32> {
  const max_num: usize = 100;

  let mut result: [i32; max_num] = [0; max_num];

  for _num in arr.iter() {
    let num = (*_num) as usize;

    result[num] = result[num] + 1;
  }

  return result.to_vec();
}