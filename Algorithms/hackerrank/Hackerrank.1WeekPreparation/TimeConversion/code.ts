function timeConversion(s: string): string {
  const inputSplited: Array<string> = s.split(":");
  const meridium: string = inputSplited[inputSplited.length - 1].substring(inputSplited[inputSplited.length - 1].length - 2, inputSplited[2].length);
  inputSplited[inputSplited.length - 1] = inputSplited[inputSplited.length - 1].substring(0, 2);

  if(meridium === "PM" && inputSplited[0] !== "12") {
    inputSplited[0] = String(parseInt(inputSplited[0]) + 12);
  } else if(meridium === "AM" && inputSplited[0] === "12") {
    inputSplited[0] = "00";
  } else {}

  return inputSplited[0] + ":" + inputSplited[1] + ":" + inputSplited[2];
}