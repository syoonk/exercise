fn timeConversion(s: &str) -> String {
  let mut inputSplited: Vec<String> = s.split(":").map(|_str| _str.to_string()).collect();

  let meridiem: String = inputSplited[2][(inputSplited[2].len() - 2)..inputSplited[2].len()].to_string();
  inputSplited[2] = inputSplited[2][..2].to_string();

  if meridiem == "PM" && inputSplited[0] != "12" {
    let hour = inputSplited[0].parse::<i32>().unwrap() + 12;

    inputSplited[0] = if hour == 24 { String::from("00") } else { hour.to_string() };
  } else if meridiem == "AM" {
    inputSplited[0] = if inputSplited[0] == "12" { "00".to_string() } else { inputSplited[0].clone() };
  } else {}

  return inputSplited[0].to_owned() + ":" + &inputSplited[1] + ":" + &inputSplited[2];
}
