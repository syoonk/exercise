fn miniMaxSum(arr: &[i32]) {
  let mut sorted = arr.clone().to_vec();
  sorted.sort();

  let mut minimum_sum: i64 = 0;
  let mut maximum_sum: i64 = 0;
  for (idx, _num) in sorted.iter().enumerate() {
    let num = *_num as i64;
    if idx < 4 {
      minimum_sum += num;
    }

    if idx > arr.len() - 5 {
      maximum_sum += num;
    }
  }

  println!("{} {}", minimum_sum, maximum_sum);
}