function miniMaxSum(arr: number[]): void {
  const sorted: number[] = arr.sort((a, b) => a - b);

  let minSum = 0;
  let maxSum = 0;
  for(let i = 0; i < sorted.length; i++) {
    if(i < 4) {
      minSum += arr[i];
    }
    if(i > sorted.length - 5) {
      maxSum += arr[i];
    }
  }

  console.log(minSum, maxSum);
}
