fn plusMinus(arr: &[i32]) {
  let num_of_elements = arr.len() as f64;

  let mut pos: f64 = 0.0;
  let mut neg: f64 = 0.0;
  let mut zero: f64 = 0.0;
  for _num in arr {
    if _num > &0 {
      pos = pos + 1.0;
    } else if _num < &0 {
      neg = neg + 1.0;
    } else {
      zero = zero + 1.0;
    }
  }

  println!("{:.6}", pos / num_of_elements);
  println!("{:.6}", neg / num_of_elements);
  println!("{:.6}", zero / num_of_elements);
}
