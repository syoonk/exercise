function plusMinus(arr: number[]): void {
  const numberOfElements = arr.length;

  let pos: number = 0;
  let neg: number = 0;
  let zero: number = 0;

  for(let _number of arr) {
    if(_number > 0) {
      pos++;
    } else if(_number < 0) {
      neg++;
    } else {
      zero++;
    }
  }

  console.log((pos / numberOfElements).toFixed(6));
  console.log((neg / numberOfElements).toFixed(6));
  console.log((zero / numberOfElements).toFixed(6));
}
