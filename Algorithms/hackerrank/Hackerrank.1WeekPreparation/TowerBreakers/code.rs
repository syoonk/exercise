fn towerBreakers(n: i32, m: i32) -> i32 {
  return if n % 2 == 0 || m == 1 { return 2 } else { return 1 };
}