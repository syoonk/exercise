function caesarCipher(s: string, k: number): string {
  const kParsed = k % 26;

  // lower: 97~122
  // upper: 65~90
  // -: 45

  let decrypted: string = "";

  for(let i = 0; i < s.length; i++) {
    const cAscii = Number(s.charCodeAt(i));

    if(cAscii >= 97 && cAscii <= 122) {
      const cAsciiTransformed = cAscii + kParsed;

      decrypted = decrypted + String.fromCharCode(cAsciiTransformed > 122 ? 96 + cAsciiTransformed - 122 : cAsciiTransformed);
    } else if(cAscii >= 65 && cAscii <= 90) {
      const cAsciiTransformed = cAscii + kParsed;

      decrypted = decrypted + String.fromCharCode(cAsciiTransformed > 90 ? 64 + cAsciiTransformed - 90 : cAsciiTransformed);

    } else {
      decrypted = decrypted + s[i];
    }
  }

  return decrypted;
}