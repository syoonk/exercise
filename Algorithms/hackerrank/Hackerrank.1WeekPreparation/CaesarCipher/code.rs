fn caesarCipher(s: &str, k: i32) -> String {
  let encrypted_chars: Vec<char> = s.chars().collect();
  let k_parsed = (k % 26) as u8;
  let mut decrypted_chars: Vec<char> = Vec::with_capacity(encrypted_chars.len());

  // lower: 97~122
  // upper: 65~90
  // -: 45
  for c in encrypted_chars {
    let c_ascii = c as u8;

    if c_ascii >= 97 && c_ascii <= 122 {
      let c_ascii_transformed = c_ascii + k_parsed;

      decrypted_chars.push(if c_ascii_transformed > 122 { (96 + c_ascii_transformed - 122) as char } else { c_ascii_transformed as char });
    } else if c_ascii >= 65 && c_ascii <= 90 {
      let c_ascii_transformed = c_ascii + k_parsed;

      decrypted_chars.push(if c_ascii_transformed > 90 { (64 + c_ascii_transformed - 90) as char } else { c_ascii_transformed as char });
    } else {
      decrypted_chars.push(c);
    }
  }

  return decrypted_chars.into_iter().collect();
}