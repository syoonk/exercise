function diagonalDifference(arr: number[][]): number {
  let forDiagonal: number = 0;
  let revDiagonal: number = 0;

  for(let i = 0; i < arr.length; i++) {
    forDiagonal += arr[i][i];
    revDiagonal += arr[i][arr.length - 1 - i];
  }

  return Math.abs(forDiagonal - revDiagonal);
}