fn diagonalDifference(arr: &[Vec<i32>]) -> i32 {
  let dimension = arr.len();
  let mut for_diagonal: i32 = 0;
  let mut rev_diagonal: i32 = 0;
  for i in (0..dimension) {
    for_diagonal = for_diagonal + arr[i][i];
    rev_diagonal = rev_diagonal + arr[i][dimension - 1 - i];
  }

  return if for_diagonal > rev_diagonal { for_diagonal - rev_diagonal } else { rev_diagonal - for_diagonal };
}