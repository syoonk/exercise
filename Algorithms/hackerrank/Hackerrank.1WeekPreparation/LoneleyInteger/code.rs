fn lonelyinteger(a: &[i32]) -> i32 {
  let mut values: HashMap<i32, u32> = HashMap::new();

  for _a in a.iter() {
    let key: i32 = *_a;
    values.entry(key).or_insert(0);
    values.insert(key, values[&key] + 1);
  }

  for (_key, _value) in values.iter() {
    if _value == &1 {
      return *_key;
    }
  }

  return 0;
}