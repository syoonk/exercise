function lonelyinteger(a: number[]): number {
  let matcher: {[k: number]: any} = {};

  for(let _a of a) {
    if(matcher[_a] === undefined) {
      matcher[_a] = true;
    } else {
      delete matcher[_a];
    }
  }

  return Number(Object.keys(matcher)[0]);
}
