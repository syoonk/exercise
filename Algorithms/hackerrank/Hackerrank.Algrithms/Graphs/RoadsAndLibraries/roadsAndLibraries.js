class Graph {
  constructor(_numOfVertex) {
    this.numOfVertices = _numOfVertex;
    this.AdjList = new Map();
    this.roads = 0;
  }

  // Initialize new vertex
  addVertex(_ver) {
    this.AdjList.set(_ver, []);
  }

  // set undirected edge
  addEdge(_verA, _verB) {
    this.AdjList.get(_verA).push(_verB);
    this.AdjList.get(_verB).push(_verA);
  }

  // dfs algorithm trigger
  dfs(_startingVertex) {
    // initialization of vertex informations
    // For the vertex is started from 1, 0 index will be empty.
    let visited = new Array(this.numOfVertices + 1);
    for(let i = 1; i <= this.numOfVertices; i++)
      visited[i] = { vertex: i, group: 0, visited: false };
    
    let group = 0;
    for(let i = 1;  i <= this.numOfVertices; i++) {
      if(visited[i].visited) continue;

      // if the current vertex is not vistied before, it is new group
      group++;
      this.dfsUtils(i, visited, group);
    }

    return ({ numOfGroup: group, numOfRoad: this.roads });
  }

  dfsUtils(_vert, _visited, _group) {
    _visited[_vert].visited = true;
    _visited[_vert].group = _group;
    
    let adjs = this.AdjList.get(_vert);
    for(let adj of adjs) {
      if(!_visited[adj].visited) {
        this.dfsUtils(adj, _visited, _group);
        this.roads++;
      }
    }
  }
}

// Complete the roadsAndLibraries function below.
function roadsAndLibraries(n, c_lib, c_road, cities) {
  // redefine variables more recognizable
  const [numOfVertex, libraryCost, roadCost, edges] =
    [n, c_lib, c_road, cities];

  // If constructing road cost is more expensive, 
  // construct library each city is always cheaper
  if(libraryCost < roadCost)  return numOfVertex * libraryCost;

  // initialize class
  let graph = new Graph(numOfVertex);
  // add vertex. vertex number starts from 1
  for(let i = 1; i <= numOfVertex; i++)
    graph.addVertex(i);
  // add edges
  for(let edge of edges) {
    graph.addEdge(edge[0], edge[1]);
  }

  const results = graph.dfs(1);
  const costs = libraryCost * results.numOfGroup + roadCost * results.numOfRoad;
  return costs;
}
