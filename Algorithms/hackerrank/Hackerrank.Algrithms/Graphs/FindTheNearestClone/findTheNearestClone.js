class Queue {
  constructor() { this.stacks = []; }

  enqueue(_element) { this.stacks.push(_element); }

  // Last in first out. Last in is pushed last index of array
  // pop takes out its last index value
  lifo() { return this.stacks.pop(); }

  // Check stacks is empty
  isEmpty() {
    if(this.stacks.length == 0) return true;
    return false;
  }
}

class Graph {
  constructor(_numOfVertex) {
    this.numOfVertices = _numOfVertex;
    this.AdjList = new Map();
  }

  // Initialize new vertex
  addVertex(_ver) {
    this.AdjList.set(_ver, []);
  }
  // Undirected edge
  addEdge(_verA, _verB) {
    this.AdjList.get(_verA).push(_verB);
    this.AdjList.get(_verB).push(_verA);
  }

  bfs(_startingVertex, _colors, _target) {
    // initialization of vertex informations
    // For the vertex is started from 1, 0 index will be empty.
    let vtxInfos = new Array(this.numOfVertices + 1);
    for(let i = 1; i <= this.numOfVertices; i++) {
      vtxInfos[i] = {vertex: i, color: _colors[i - 1], visited: false};
    }

    // create an instance of queue
    let queue = new Queue();
    let result = 0;
    for(let i = 1; i <= this.numOfVertices; i++) {
      // only target color is considered as root node
      if(vtxInfos[i].color === _target) {
        // this is the root node, edge distance starts from 0
        let edgesCount = 0;
        // newly count distance, reinitialize visited status
        for(let i = 1; i <= this.numOfVertices; i++) { vtxInfos[i].visited = false; }
        
        vtxInfos[i].visited = true;
        queue.enqueue(i);
      }
      // loop until out of queue
      while(!queue.isEmpty()) {
        const curVer = queue.lifo();

        // count the current distance
        edgesCount++;

        const adjs = this.AdjList.get(curVer);

        for(let adj of adjs) {
          if(vtxInfos[adj].visited) continue;

          if(vtxInfos[adj].color === _target) {
            // if edgesCount is 1, it is closest one
            if(edgesCount === 1) return edgesCount;
            
            // put smaller one to result to find minimum distance
            (result === 0 || edgesCount < result) && (result = edgesCount);
          }

          vtxInfos[adj].visited = true;
          queue.enqueue(adj);
        }
      }
    }

    return result;
  }
}

function findShortest(graphNodes, graphFrom, graphTo, ids, val) {
  console.log(graphNodes, graphFrom, graphTo, ids, val);

  let graph = new Graph(graphNodes);
  // add vertex, vertex number starts from 1
  for(let i = 1; i <= graphNodes; i++) graph.addVertex(i);
  // add edges
  for(let i = 0; i < graphFrom.length; i++)  graph.addEdge(graphFrom[i], graphTo[i]);

  const result = graph.bfs(1, ids, val);

  if(result === 0) return -1;
  return result;
}
