function maximumToys(prices, k) {
  let maximumGreedy = 0;
  let cumPrice = 0;
  prices.sort((x, y) => x - y);

  for(let price of prices) {
    cumPrice += price;
    if(cumPrice >= k) break;
    maximumGreedy++;
  }

  return maximumGreedy;
}