function activityNotifications(expenditure, d) {
  const calMedian = (arr, maxIndex) => {
    const middleIndex = parseInt(maxIndex / 2);
    const even = (maxIndex % 2 === 0);
    let acc = 0;
    
    if(even) {
      for(let i = 0; i < 201; i++) {
        acc += arr[i];
        if(acc === middleIndex) {
          for(let j = i + 1; j < 201; j++) {
            if(arr[j] > 0) {
              return (i + j) / 2;
            }
          }
        } else if(acc > middleIndex) {
          return i;
        }
      }
    } else {
      for(let i = 0; i < 201; i++) {
        acc += arr[i];
        if(acc === middleIndex) {
          for(let j = i + 1; j < 201; j++) {
            if(arr[j] > 0)  return j;
          }
        } else if(acc > middleIndex) {
          return i;
        }
      }
      console.log("This is an ERROR");
    }
  }
  let numExps = new Array(201).fill(0);

  // Non-trailing days
  for(let i = 0; i < d; i++) {
    numExps[expenditure[i]]++;
  }
  let median = calMedian(numExps, d);
  let notified = 0;

  for(let i = d; i < expenditure.length; i++) {
    if(expenditure[i] >= median * 2)  notified++;
    numExps[expenditure[i]]++;
    numExps[expenditure[i - d]]--;
    median = calMedian(numExps, d);
  }

  return notified;
}