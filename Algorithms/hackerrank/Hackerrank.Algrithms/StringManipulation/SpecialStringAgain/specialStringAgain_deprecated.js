/*
  1. This is for normal palindrome not for special palindrome that
  special palindromic condition of this exercise.
  2. This is > O(n), and it takes much time. 
*/

function substrCount(n, s) {
  let count = s.length;

  for(let i = 0; i < s.length - 1; i++) {
    for(let j = s.length; j > i; j--) {
      let matched = false;
      s[i] === s[j] ? matched = true : matched = false;

      const gap = (s.length - i) / 2;
      const k = j;
      let g = 0;
      while(g < gap && matched === true) {
        if(s[i + g] !== s[j - g]) {
          matched = false;
          break;
        }
        g++;
      }
      if(matched === true) count++;
    }
  }

  return count;
}