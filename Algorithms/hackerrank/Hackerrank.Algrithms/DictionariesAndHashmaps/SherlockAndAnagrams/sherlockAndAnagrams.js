function sherlockAndAnagrams(s) {
    var anam = {};
    var count = 0;

    // slice(a,b) (a < b)
    for (let i = 0; i < s.length; i++) {
        for (let j = i + 1; j < s.length + 1; j++) {
            let substr = s.slice(i, j).split('').sort().join('');

            if (anam[substr] > 0) {
                count += anam[substr];
                anam[substr]++;
            } else {
                anam[substr] = 1;
            }
        }
    }

    return count;
}