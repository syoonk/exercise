/*

    This code is not working for r = 1 case..

*/

'use strict';

const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', function (inputStdin) {
    inputString += inputStdin;
});

process.stdin.on('end', function () {
    inputString = inputString.split('\n');

    main();
});

function readLine() {
    return inputString[currentLine++];
}

// Complete the countTriplets function below.
function countTriplets(arr, r) {
    let p = 0;
    let pow = Math.pow(r, p);
    let mapping = [];

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] > pow) {
            p++;
            pow = Math.pow(r, p);
        }

        // console.log(`before mapping || arr: ${arr}, pow: ${pow}`);

        if (arr[i] == pow) {
            if (!mapping[p]) {
                mapping[p] = 1;
            } else {
                mapping[p]++;
            }
        }
    }

    // console.log(mapping);

    let count = 0;

    for (let i = 0; i < mapping.length - 1; i++) {
        // console.log(`i = ${i}, mapping : ${mapping[i]}, ${mapping[i+1]}, ${mapping[i+2]}`);

        if (!mapping[i + 2]) { return count; }

        count += (mapping[i] * mapping[i + 1] * mapping[i + 2]);
    }

    return count;
}

function main() {
    const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

    const nr = readLine().replace(/\s+$/g, '').split(' ');

    const n = parseInt(nr[0], 10);

    const r = parseInt(nr[1], 10);

    const arr = readLine().replace(/\s+$/g, '').split(' ').map(arrTemp => parseInt(arrTemp, 10));

    const ans = countTriplets(arr, r);

    ws.write(ans + '\n');

    ws.end();
}
