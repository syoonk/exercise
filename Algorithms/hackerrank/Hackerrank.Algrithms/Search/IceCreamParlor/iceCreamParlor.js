function whatFlavors(cost, money) {
  let costIndex = new Map();  //  (cost, index)

  for(let i = 0; i < cost.length; i++) {
    if(cost[i] >= money) continue;

    const remain = money - cost[i];
    if(costIndex.has(remain)) {
      console.log(costIndex.get(remain), i + 1);
      return ;
    }
    
    costIndex.set(cost[i], i + 1);
  }
}