function triplets(a, b, c) {
  for(let arr of [a, b, c]) {
    arr.sort((x, y) => x - y);
  }

  let count = 0;
  for(let i_b = 0; i_b < b.length; i_b++) {
    if(b[i_b] === b[i_b - 1]) continue;

    let count_a = 0;
    let count_c = 0;

    for(let i_a = 0; i_a < a.length; i_a++) {
      if(a[i_a] === a[i_a - 1]) continue;
      if(a[i_a] > b[i_b]) break;
      count_a++;
    }
    for(let i_c = 0; i_c < c.length; i_c++) {
      if(c[i_c] === c[i_c - 1]) continue;
      if(c[i_c] > b[i_b]) break;
      count_c++;
    }

    count += count_a * count_c;
  }
  
  return count;
}
