function triplets(a, b, c) {
  [a, b, c] = [a, b, c].map(arr => (
    [...new Set(arr.sort((x, y) => x - y))]
  ));

  let [count, aCount, cCount] = [0, 0, 0];
  for(let i = 0; i < b.length; i++) {
    while(aCount < a.length && a[aCount] <= b[i]) aCount++;
    while(cCount < c.length && c[cCount] <= b[i]) cCount++;
    count += aCount * cCount;
  }  

  return count;
}