/*
    -9 -9 -9  1 1 1
    0 -9  0  4 3 2
    -9 -9 -9  1 2 3
     0  0  8  6 6 0
    0  0  0 -2 0 0
    0  0  1  2 4 0

    Derive hourglass array from above like a example below and sum..

    0 4 3
      1
    8 6 6

    this sum is 28
    There would be 16 hourglass array. and sum of all value of hourglass array.
    And compare the values and return the maximum value among them.
*/

function eachSum(arr, i, j) {
    var sum = 0;

    for (let x = i - 1; x <= i + 1; x++) {
        for (let y = j - 1; y <= j + 1; y = y + 2) {
            sum += arr[y][x];
        }
    }
    return (sum + arr[j][i]);
}

function hourglassSum(arr) {
    const N = arr.length;
    var maxHourGlass = (-1) * (9 * 36); // calculated from constraints...
    var dummy;

    for (let i = 1; i < N - 1; i++) {
        for (let j = 1; j < N - 1; j++) {
            dummy = eachSum(arr, i, j);
            if (dummy > maxHourGlass) { maxHourGlass = dummy; }
        }
    }

    return maxHourGlass;
}