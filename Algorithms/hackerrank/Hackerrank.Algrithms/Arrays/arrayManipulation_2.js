/*
    Starting with a 1-indexed array of zeros and a list of operations, for each operation
    add a value to each of the array element between two given indices, inclusive. Once all
    operations have been performed, return the maximum value in your array.

    Explanation::
    Input:
    5 3
    1 2 100
    2 5 100
    3 4 100

    Result: 200

    After the first update list will be 100 100 0 0 0.
    After the second update list will be 100 200 100 100 100.
    After the third update list will be 100 200 200 200 100.
    The required answer will be 200.

*/

function calculateMax(arr) {
    let max = 0;

    arr.reduce((a, b) => {
        let sum = a + b;

        if (sum > max) {
            return max = sum;
        }

        return sum;
    });

    return max;
}

function arrayManipulation(n, queries) {
    var oper = new Array(n + 1).fill(0);

    for (let i = 0; i < queries.length; i++) {
        oper[queries[i][0] - 1] += queries[i][2];
        oper[queries[i][1]] -= queries[i][2];
    }

    var max = calculateMax(oper);

    return max;
}
