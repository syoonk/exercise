/*
    Given array a, shift it amount d to the left.

    for example : a = [1, 2, 3, 4, 5], d = 2
    result = [3, 4, 5, 1, 2] 
*/

function rotLeft(a, d) {
    const N = a.length;
    var shifted = [];

    for (let i = 0; i < N; i++) {
        if (i - d < 0) shifted[N + i - d] = a[i];
        else shifted[i - d] = a[i];
    }

    return shifted;
}