/*
    It's New Year's Day and everyone's in line for the Wonderland rollercoaster ride! There are a number of people 
    queued up, and each person wears a sticker indicating their initial position in the queue. 
    Initial positions increment by 1 from 1 at the front of the line to n at the back.
    Any person in the queue can bribe the person directly in front of them to swap positions. If two people 
    swap positions, they still wear the same sticker denoting their original places in line. One person can 
    bribe at most two others. 
    For example, if n = 8 and Person 5 bribes Person 4, the queue will look like this: 1,2,3,5,4,6,7,8.

    Fascinated by this chaotic queue, you decide you must know the minimum number of bribes that took place 
    to get the queue into its current state!

    
    Sample Input:
    2
    5
    2 1 5 3 4
    5
    2 5 1 3 4

    Sample Output:
    3
    Too chaotic

*/

function minimumBribes(q) {
    const N = q.length;
    var nBribe = 0;
    var indexRef = [];

    for (let i = N - 1; i >= 0; i--) {
        if (((--q[i]) - i) > 2) {
            console.log('Too chaotic');
            return;
        }

        indexRef[q[i]] = i;
    }

    for (let i = N - 1; i >= 0; i--) {
        while (q[i] != i) {
            let dummy = indexRef[i];
            let dummyq = q[dummy + 1];

            q[dummy + 1] = i;
            q[indexRef[i]] = dummyq;

            indexRef[i]++;
            indexRef[dummyq]--;

            nBribe++;
        }
    }

    console.log(nBribe);
}