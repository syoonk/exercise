/*
  It distribute the buy amount acending order to lower price.
  I'm not sure if it will pass very special case either, it passed all test cases anyway
  ===> It works with any case because there is constraint that one cannot buy same flower again.
*/
function getMinimumCost(k, c) {
  c.sort((x, y) => y - x);
  let eachBuyAmount = c.map((el, idx) => parseInt(idx / k) + 1);
  
  let sum = 0;
  for(let i = 0; i < c.length; i++) {
    sum += c[i] * eachBuyAmount[i];
  }

  return sum;
}