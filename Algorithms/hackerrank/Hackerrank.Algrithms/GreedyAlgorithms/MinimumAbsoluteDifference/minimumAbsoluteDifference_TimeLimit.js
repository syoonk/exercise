/*
  It goes past time limit.
*/
function minimumAbsoluteDifference(arr) {
  let minimumAbs = Math.abs(arr[0] - arr[1]);
  
  for(let i = 0; i < arr.length - 1; i++) {
    for(let j = i + 1; j < arr.length; j++) {
      let abs = Math.abs(arr[i] - arr[j]);
      minimumAbs = abs > minimumAbs ? minimumAbs : abs;
    }
  }
  return minimumAbs;
}