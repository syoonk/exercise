function minimumAbsoluteDifference(arr) {
  arr.sort((x,y) => x - y);
  let minimumAbs = Math.abs(arr[0] - arr[1]);
  
  for(let i = 1; i < arr.length - 1; i++) {
    let abs = Math.abs(arr[i] - arr[i + 1]);
    if(abs === 0) return 0;
    minimumAbs = abs > minimumAbs ? minimumAbs : abs;
  }
  return minimumAbs;
}