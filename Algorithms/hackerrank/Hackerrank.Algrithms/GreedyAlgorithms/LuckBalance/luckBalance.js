function luckBalance(k, contests) {
  const sortFunction = (a, b) => {
    if(a[0] === b[0]) {
      return 0;
    } else {
      return (a[0] > b[0]) ? -1 : 1;
    }
  }
  contests.sort(sortFunction);
  
  let luckSum = 0;
  let loseCount = 0;
  for(let contest of contests) {
    console.log(contest[0]);
    if(contest[1] === 0) {
      luckSum += contest[0];
    }
    if(contest[1] === 1) {
      if(loseCount < k) {
        luckSum += contest[0];
        loseCount++;
      } else {
        luckSum -= contest[0];
      }
    }
  }

  return luckSum;
}