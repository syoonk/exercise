function candies(n, arr) {
  let candies = new Array(arr.length).fill(0);
  candies[0] = 1;
  for(let i = 1; i < n; i++) {
    candies[i] = arr[i] > arr[i - 1] ? candies[i - 1] + 1 : 1;
  }

  let totalCandyBackward = candies[n - 1];
  for(let i = n - 2; i >= 0; i--) {
    candies[i] = (arr[i] > arr[i + 1] && candies[i] <= candies[i + 1]) ? candies[i + 1] + 1 : candies[i];
    totalCandyBackward += candies[i];
  }

  return totalCandyBackward;
}