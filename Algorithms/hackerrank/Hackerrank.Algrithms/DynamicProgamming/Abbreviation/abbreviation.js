function abbreviation(a, b) {
  //32 , ASCII: 65~90 ascii: 97~122
  const isUpper = (a) => {
    return (a >= 65 && a <= 90);
  }

  const isMatched = (chrA, chrB) => {
    if(chrB === " " || typeof(chrB) === 'undefined') {
      return false;
    }
    return ((chrA.charCodeAt(0) - 32) === chrB.charCodeAt(0) || chrA === chrB);
  }

  // Initializtion
  let grid = new Array(a.length + 1);
  for(let i = 0; i < a.length + 1; i++) {
    grid[i] = new Array(b.length + 1).fill(0);
  }
  grid[0][0] = 1;
  a = " " + a;
  b = " " + b;

  for(let ai = 1; ai < a.length; ai++) {
    for(let bi = 0; bi < ai + 1; bi++) {
      const upper = isUpper(a[ai].charCodeAt(0));
      if(upper) {
        (isMatched(a[ai], b[bi]) && grid[ai - 1][bi - 1]) && (grid[ai][bi] = 1);
      }
      if(!upper) {
        if(grid[ai - 1][bi] === 1) {
          grid[ai][bi] = 1;
          continue;
        }
        (isMatched(a[ai], b[bi]) && grid[ai - 1][bi - 1]) && (grid[ai][bi] = 1);
      }
    }
  }

  return (grid[a.length - 1][b.length - 1]) ? "YES" : "NO";
}