/*
    Lilah has a string, s, of lowercase English letters that she repeated infinitely many times.
    Given an integer, n, find and print the number of letter a's in the first n letters of Lilah's infinite string.
    For example, if the string s = 'abcac' and n=10, the substring we consider is abcacabcac, 
    the first 10 characters of her infinite string. There are 4 occurrences of a in the substring.
*/

function repeatedString(s, n) {
    var nA = 0;
    var rA = 0;
    var nRep = parseInt(n / s.length);
    var pStr = n % s.length;

    for (let i in s) {
        if (s[i] == 'a') nA++;
    }

    for (let i in s.slice(0, pStr)) {
        if (s[i] == 'a') rA++;
    }

    return (nA * nRep + rA);
}