/*
    John works at a clothing store. He has a large pile of socks that he must pair by color for sale. 
    Given an array of integers representing the color of each sock, determine how many pairs of socks 
    with matching colors there are.

    For example, there are n=7 socks with colors ar = [1, 2, 1, 2, 1, 3, 2]. 
    There is one pair of color 1 and one of color 2. The number of pairs is 2.
*/

function sockMerchant(n, ar) {
    var sorting = {};
    var sorted = 0;

    for (let i in ar) {
        if (sorting[ar[i]]) {
            sorted++;
            sorting[ar[i]] = 0;
        } else { sorting[ar[i]] = 1; }
    }

    return sorted;
}
