/*
 * For your reference:
 *
 * DoublyLinkedListNode {
 *     int data;
 *     DoublyLinkedListNode next;
 *     DoublyLinkedListNode prev;
 * }
 *
 */
function reverse(head) {
  let currentNode = head;
  
  let dataQueues = [];
  while(true) {
    dataQueues.push(currentNode.data);

    if(currentNode.next === null) break;

    currentNode = currentNode.next
  }

  let reversedList = new DoublyLinkedListNode(dataQueues.pop());
  currentNode = reversedList;
  while(dataQueues.length > 0) {
    currentNode.next = new DoublyLinkedListNode(dataQueues.pop());
    currentNode = currentNode.next;
  }

  return reversedList;
}
