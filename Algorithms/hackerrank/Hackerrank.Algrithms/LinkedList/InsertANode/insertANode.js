/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
function insertNodeAtPosition(head, data, position) {
  let currentNode = head;
  let pos = 1;
  while(1) {
    if(pos === position) {
      let temp = currentNode.next;
      currentNode.next = new SinglyLinkedListNode(data);
      currentNode.next.next = temp;

      break;
    }

    currentNode = currentNode.next;
    pos++;
  }

  return head;
}