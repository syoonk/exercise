/*
    Find merge point of two linked lists
    Note that the head may be 'null' for the empty list.
    Node is defined as
    var Node = function(data) {
        this.data = data;
        this.next = null;
    }
*/

// This is a "method-only" submission.
// You only need to complete this method.

function findMergeNode(headA, headB) {
  let currentNodeA = headA.next;
  let nodeADatas = [];
  
  while(currentNodeA) {
    nodeADatas.push(currentNodeA.data);

    if(currentNodeA.next === null)  break;
    
    currentNodeA = currentNodeA.next;
  }

  let currentNodeB = headB.next;
  let nodeBDatas = [];

  while(currentNodeB) {
    nodeBDatas.push(currentNodeB.data);
    
    if(currentNodeB.next === null)  break;
    
    currentNodeB = currentNodeB.next;
  }

  let mergePoint = undefined;
  while(nodeADatas.length > 0 && nodeBDatas.length > 0) {
    const nodeAData = nodeADatas.pop();
    const nodeBData = nodeBDatas.pop();
    
    if(nodeAData !== nodeBData) return mergePoint;
    
    mergePoint = nodeAData;
  }
  
  return mergePoint;
}
