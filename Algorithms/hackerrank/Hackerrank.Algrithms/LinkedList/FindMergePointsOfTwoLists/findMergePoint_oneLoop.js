/*
    Find merge point of two linked lists
    Note that the head may be 'null' for the empty list.
    Node is defined as
    var Node = function(data) {
        this.data = data;
        this.next = null;
    }
*/

// This is a "method-only" submission.
// You only need to complete this method.

function findMergeNode(headA, headB) {
  let currentNodeA = headA.next;
  let currentNodeB = headB.next;

  while(currentNodeA !== currentNodeB) {
    currentNodeA = currentNodeA.next ? currentNodeA.next : headB.next;
    currentNodeB = currentNodeB.next ? currentNodeB.next : headA.next;
  }

  return currentNodeA.data;
}