/*
 * For your reference:
 *
 * DoublyLinkedListNode {
 *     int data;
 *     DoublyLinkedListNode next;
 *     DoublyLinkedListNode prev;
 * }
 *
 */
function sortedInsert(head, data) {
  let newNode = new DoublyLinkedListNode(data);
  newNode.prev = null; newNode.next = null;

  let currentNode = head;

  // if new node is placed first, return newNode, it should be out of while
  if(currentNode.data >= data) {
    const temp = currentNode;
    currentNode = newNode;
    temp.prev = newNode;
    newNode.next = temp;

    return newNode;
  }

  // new node into mid of trees or last
  while(true) {
    if(currentNode.next === null) {
      newNode.prev = currentNode;
      currentNode.next = newNode;

      return head;
    }

    if(currentNode.next.data >= data) {
      const temp = currentNode.next;
      newNode.next = temp;
      currentNode.next = newNode;
      
      return head;
    }

    currentNode = currentNode.next;
  }
}
