function isBalanced(s) {
  // (): 40, 41 []: 91, 93 {}: 123, 125
  if(s.length % 2 !== 0) { return 'NO' }

  let openBracketList = [ 40, 91, 123 ];
  const closeBracketSet = { 41: 40, 93: 91, 125: 123 };
  
  let currentOpenedBracket = [];
  for(let i = 0; i < s.length; i++) {
    const asciiNum = s.charCodeAt(i);
    if(openBracketList.some(key => Number(key) === asciiNum)) {
      currentOpenedBracket.push(asciiNum);
    } else {
      if(closeBracketSet[asciiNum] !== currentOpenedBracket[currentOpenedBracket.length - 1]) { return "NO"; }
      currentOpenedBracket.pop();
    }
  }

  return currentOpenedBracket.length === 0 ? "YES" : "NO";
}