/*
  It solves all the cases except only case 2 for time limit.
*/

function riddle(arr) {
  const maxSize = arr.length;
  let results = Array(maxSize).fill(0);

  for(let i = 0; i < maxSize; i++) {
      let q = 0; let queues = []; let minimalWindow = 10e+9;
      while(q < (maxSize - i)) {
        (minimalWindow > arr[i + q]) && (minimalWindow = arr[i + q]);
        (minimalWindow > results[q]) && (results[q] = minimalWindow);
        q++;
    }
  }

  return results;
}