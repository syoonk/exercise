function processData(input) {
  //Enter your code here
  const queueTypes = {
    "1" : function(props) {
      props.arr.push(props.val);
      return props.arr;
    },
    "2" : function(props) {
      props.arr.shift();
      return props.arr;
    },
    "3" : function(props) {
      console.log(props.arr[0]);
      return props.arr;
    }
  }
  
  let inputs = input.split("\n");
  let queue = [];
  for(let i = 1; i < inputs.length; i++) {
    const values = inputs[i].split(" ");
    queue = queueTypes[values[0]]({val: values[1], arr: queue });
  }
} 