/*
  Needs some refactoring
*/

function minimumMoves(grid, startX, startY, goalX, goalY) {
  const setRoot = (_x, _y) => {
    return { rootX: _x, rootY: _y };
  };
  const setQueue = (_x, _y) => {
    return { queueX: _x, queueY: _y };
  };
  
  const N = grid.length;
  grid = grid.map(g => g.split(''));
  let tread = Array.from(Array(N), () => Array(N).fill(false)); 
  tread[startX][startY] = true;
  const xVector = [1, 0, -1, 0], yVector = [0, 1, 0, -1];
  let queues = [setQueue(startX, startY)];

  let x, y, i, j;
  let goalReaches = false;
  while(!goalReaches) {
    const queue = queues.shift();
    i = queue.queueX; j = queue.queueY;
    for(let v = 0; v < 4; v++) {
      for(let n = 1; n < N; n++) {
        [x, y] = [i + xVector[v] * n, j + yVector[v] * n];
        if(x < 0 || x >= N || y < 0 || y >= N || grid[x][y] === 'X')  break;

        if(tread[x][y] === false) {
          tread[x][y] = setRoot(i, j);
          queues.push(setQueue(x, y));
        }

        if(x === goalX && y === goalY) {
          goalReaches = true;
          break;
        }
        if(goalReaches) break;
      }
    }
  }

  let root = tread[goalX][goalY];
  let [rx, ry] = [root.rootX, root.rootY];
  let count = 1;
  while(rx !== startX || ry !== startY) {
    root = tread[rx][ry];
    [rx, ry] = [root.rootX, root.rootY];
    count++;
    
    if(rx === startX && ry === startY)  break;
  }

  return count;
}