function largestRectangle(h) {
  let largestArea = 0;

  for(let i = 0; i < h.length; i++) {
    let rightStacks = 0;
    while(rightStacks + i + 1 < h.length && h[i + rightStacks + 1] >= h[i]) {
      rightStacks++;
    }

    let leftStacks = 0;
    while(i - leftStacks - 1 >= 0 && h[i - leftStacks - 1] >= h[i]) {
      leftStacks++;
    }
    const length = rightStacks + leftStacks + 1;
    const possibleArea = length * h[i];
    largestArea = largestArea < possibleArea ? possibleArea : largestArea;
  }
  
  return largestArea;
}
