/*
  Complete the function in the editor. It has one parameter: an array, nums.
  It must iterate through the array performing one of the following actions
  on each element:
  1. If the element is even, multiply the element by 2.
  2. If the element is odd, multiply the element by 3.
  3. The function must then return the modified array.
*/

'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Modify and return the array so that all even elements are doubled and all odd elements are tripled.
 *
 * Parameter(s):
 * nums: An array of numbers.
 */
function modifyArray(nums) {
    var modNums = [];

    nums.forEach(elements  => elements%2 ? modNums.push(elements*3):modNums.push(elements*2));

    return modNums;
}


function main() {
    const n = +(readLine());
    const a = readLine().split(' ').map(Number);

    console.log(modifyArray(a).toString().split(',').join(' '));
}
