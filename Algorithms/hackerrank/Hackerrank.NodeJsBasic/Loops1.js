'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the vowelsAndConsonants function.
 * Print your output using 'console.log()'.
 */
function vowelsAndConsonants(s) {
//    s.forEach(element => console.log(element));
    var v = new Array();
    var c = new Array();

    for(let i in s) {
        if(s[i] == 'a'
        || s[i] == 'e'
        || s[i] == 'i'
        || s[i] == 'o'
        || s[i] == 'u') {
          v.push(s[i]);
        } else {
            c.push(s[i]);
        }
    }
    for(let i in v) { console.log(v[i]); }
    for(let i in c) { console.log(c[i]); }
}


function main() {
    const s = readLine();

    vowelsAndConsonants(s);
}
