class Queue {
  constructor() {
    this.stacks = [];
  }

  enqueue(_element) {
    console.log("Enqueue:: ", _element);
    this.stacks.push(_element);
  }

  // Last in first out. Last in is pushed last index of array
  // pop takes out its last index value
  lifo() {
    const output = this.stacks.pop();
    console.log("LIFO Dequeue:: ", output);
    return output;
  }

  // First in First out. First in is first index of array
  // shift takes out its first index value
  fifo() {
    const output = this.stacks.pop();
    console.log("FIFO Dequeue:: ", output);
    return output;;
  }

  // Check stacks is empty
  isEmpty() {
    if(this.stacks.length == 0) return true;
    return false;
  }
}

class Graph {
  constructor(_numOfVertex) {
    this.numOfVertices = _numOfVertex;
    this.AdjList = new Map();
  }

  // Initialize new vertex
  addVertex(_ver) {
    this.AdjList.set(_ver, []);
  }
  // Undirected edge
  addEdge(_verA, _verB) {
    this.AdjList.get(_verA).push(_verB);
    this.AdjList.get(_verB).push(_verA);
  }
  // Prints the vertex and adjacency AdjLis
  printGraph() {
    let keys = this.AdjList.keys();

    for(let key of keys) {
      let printText = "";
      let values = this.AdjList.get(key);
      
      for(let value of values) {
        printText += value + " ";
      }

      console.log(`${key} -> ${printText}`);
    }
  }

  bfs(_startingVertex) {
    console.log("\n /**");
    console.log("** BFS");
    console.log("****");
    // initialization of vertex informations
    // For the vertex is started from 1, 0 index will be empty.
    let visited = new Array(this.numOfVertices + 1);
    for(let i = 1; i <= this.numOfVertices; i++) {
      visited[i] = { vertex: i, group: 0, visited: false };
    }
    console.log("Initialized Vertexes: ", visited);

    // create an instance of queue
    let queue = new Queue();

    let group = 0;
    for(let i = 1; i <= this.numOfVertices; i++) {
      console.log(`First For loop of ${i}, its visited is ${visited[i].visited}`);
      if(visited[i].visited) {
        console.log("vertex ", i, " is already visited. Continue to next vertex");
        continue;
      }
      // if the current vertex is not visited, then it is new group
      group++;
      visited[i].visited = true;
      visited[i].group = group;
      queue.enqueue(i);
      
      // loop until out of queue
      while(!queue.isEmpty()) {
        const curVer = queue.lifo();

        const adjs = this.AdjList.get(curVer);
        for(let adj of adjs) {
          if(visited[adj].visited) continue;

          visited[adj].visited = true;
          visited[adj].group = group;
          queue.enqueue(adj);
        }
      }
    }

    console.log("Finished Vertices: ", visited);
  }

  dfs(_startingVertex) {
    // initialization of vertex informations
    // For the vertex is started from 1, 0 index will be empty.
    let visited = new Array(this.numOfVertices + 1);
    for(let i = 1; i <= this.numOfVertices; i++) visited[i] = { vertex: i, group: 0, visited: false };
    console.log("Initialized Vertexes: ", visited);
    
    let group = 0;
    for(let i = 1;  i <= this.numOfVertices; i++) {
      if(visited[i].visited) {
        console.log("vertex ", i, " is already visited. Continue to next vertex");
        continue;
      }

      // if the current vertex is not vistied before, it is new group
      group++;
      this.dfsUtils(i, visited, group);
    }

    console.log("Finished Vertices: ", visited);
  }

  dfsUtils(_vert, _visited, _group) {
    _visited[_vert].visited = true;
    _visited[_vert].group = _group;
    console.log("Current vertex: ", _vert);
    
    let adjs = this.AdjList.get(_vert);
    for(let adj of adjs) {
      if(!_visited[adj].visited) {
        this.dfsUtils(adj, _visited, _group);
      }
    }
  }
}

// const edges = [ [1, 2], [2, 3], [3, 4], [4, 5], [5, 1], [5, 2] ];
// const N = 5;
const edges = [ [ 1, 2 ], [ 1, 3 ], [ 4, 5 ], [ 4, 6 ] ];
const N = 6;

let graph = new Graph(N);
// add vertex
for(let i = 1; i <= N; i++) {
  graph.addVertex(i);
}
// add edges
for(let edge of edges) {
  graph.addEdge(edge[0], edge[1]);
}

graph.printGraph();
console.log("Start Dfs");
graph.dfs(1);
graph.bfs(1);

/** Result
 * 
 * 
1 -> 2 3
2 -> 1
3 -> 1
4 -> 5 6
5 -> 4
6 -> 4
Start Dfs
Initialized Vertexes:  [
  <1 empty item>,
  { vertex: 1, group: 0, visited: false },
  { vertex: 2, group: 0, visited: false },
  { vertex: 3, group: 0, visited: false },
  { vertex: 4, group: 0, visited: false },
  { vertex: 5, group: 0, visited: false },
  { vertex: 6, group: 0, visited: false }
]
Current vertex:  1
Current vertex:  2
Current vertex:  3
vertex  2  is already visited. Continue
vertex  3  is already visited. Continue
Current vertex:  4
Current vertex:  5
Current vertex:  6
vertex  5  is already visited. Continue
vertex  6  is already visited. Continue
Finished Vertexes:  [
  <1 empty item>,
  { vertex: 1, group: 1, visited: true },
  { vertex: 2, group: 1, visited: true },
  { vertex: 3, group: 1, visited: true },
  { vertex: 4, group: 2, visited: true },
  { vertex: 5, group: 2, visited: true },
  { vertex: 6, group: 2, visited: true }
]
 * 
 */