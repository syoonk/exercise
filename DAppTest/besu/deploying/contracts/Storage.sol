pragma solidity ^0.6.7;

contract Storage {
  uint public data;
  bytes32 public key;
  string public stringData;
  struct Yes {
    uint id;
    string contents;
  }

  mapping(uint => Yes) public yes;

  constructor() public {
    yes[0] = Yes(0, "content0");
    yes[1] = Yes(1, "content0");
    yes[2] = Yes(2, "content0");

  }

  function set(uint _data)
  external {
    data = _data;
  }

  function bytesSet(bytes32 _data)
  external {
    key = _data;
  }

  function stringSet(string calldata _data)
  external {
    stringData = _data;
  }
}
