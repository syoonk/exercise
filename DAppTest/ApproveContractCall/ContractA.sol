pragma solidity ^0.6.6;

contract ContractA {
    uint changeable;
    address approved;
    
    function approveContract(address _approve) 
    external {
        approved = _approve;
    }
    
    function set(uint _externalData)
    external {
        require(msg.sender == approved,
            "fuck you");
        changeable = _externalData;
    }
    
    function get()
    external view 
    returns(uint _changeable) {
        _changeable = changeable;
    }
}