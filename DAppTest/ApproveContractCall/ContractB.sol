pragma solidity ^0.6.7;

import "./ContractA.sol";

contract ContractB {
    ContractA contractA;
    
    constructor(address _contractA)
    public {
        contractA = ContractA(_contractA);
    }
    
    function set(uint _setData)
    external {
        contractA.set(_setData);
    }
}