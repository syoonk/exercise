const crypto = require('crypto');
const EthCrypto = require('eth-crypto');

// This is wallet address that derived from public key... 
// const bobPublicKey = '0x18943Dc890114C4b21E5f2F426f978fB79096eAB';
// const alicePublicKey = '0x9Ee10e75d2272393aC57fAB83B221f77dec178F0';
const bobPrivateKey = '0x908e9f8fa5272efe0bbddf23e451e2d24d86a1433c0c73d12d518668cafcf148';
const alicePrivateKey = '0xab9876e391ce92c23581d1f368b88ef983b4f177f97bf1f044b42c7c51995f3b';
const curveName = 'secp256k1';
const algorithm = 'aes192';
const MESSAGE = "What are you";

async function doEncrypt() {
  // const curves = await crypto.getCurves();
  // console.log(curves);  

  const getEncryptAlgorithmInHex = () => {
    return '0x' + Buffer.from(algorithm, 'ascii').toString('hex');
  }

  const hexStringToAsciiString = (_hexString) => {
    if(_hexString.startsWith('0x')) {
      _hexString = _hexString.substr(2);
    }
    return Buffer.from(_hexString, 'hex').toString('ascii').replace(/\0/g, '');
  }

  const privateToPublic = (_privateKey) => {
    if(_privateKey.startsWith('0x')) {
      _privateKey = _privateKey.substr(2);
    }
    let publicKey = crypto.createECDH('secp256k1');
    publicKey.setPrivateKey(_privateKey, 'hex');
    return publicKey.getPublicKey('hex');
  }

  const computeSecret = (_privateKeyA, _publicKeyB) => {
    if(_privateKeyA.startsWith('0x')) {
      _privateKeyA = _privateKeyA.substr(2);
    }
    let G = crypto.createECDH('secp256k1');
    G.setPrivateKey(_privateKeyA, 'hex');
    return G.computeSecret(_publicKeyB, 'hex', 'hex').slice(1);
  }

  const encrypt = (_message, _secret) => {
    let cipher = crypto.createCipher(algorithm, _secret);
    let crypted = cipher.update(_message, 'utf8', 'hex');
    console.log('crypted:: ', crypted);
    crypted += cipher.final('hex');
    console.log('+crypted:: ', crypted);
    return crypted;
  }

  const decrypt = (_encryptedMessage, _secret) => {
    let decipher = crypto.createDecipher(algorithm, _secret);
    let dec = decipher.update(_encryptedMessage, 'hex', 'utf8');
    console.log('dec:: ', dec);
    dec += decipher.final('utf8');
    console.log('dec:: ', dec);
    return dec;
  }

  // const alice = crypto.createECDH('secp256k1');
  // alice.generateKeys();
  // console.log(alice);

  // const alicePrivateKeyAscii = hexStringToAsciiString(alicePrivateKey);
  // console.log(alicePrivateKeyAscii);

  const alicePublicKey = privateToPublic(alicePrivateKey);
  console.log("alicePublic::: ", alicePublicKey);
  const bobPublicKey = privateToPublic(bobPrivateKey);
  console.log("bobPublic::: ", bobPublicKey);

  // Alice send the message...
  const aliceMadeSecret = computeSecret(alicePrivateKey, bobPublicKey);
  console.log("Alice's secret:: ", aliceMadeSecret);
  const messageEncrypted = encrypt(MESSAGE, aliceMadeSecret);
  console.log("Message that alice encrypted::: ", messageEncrypted);
  
  // Bob receive the message...
  const bobMadeSecret = computeSecret(bobPrivateKey, alicePublicKey);
  console.log("Bob's secret:: ", bobMadeSecret);
  const messageDecrypted = decrypt(messageEncrypted, bobMadeSecret);
  console.log(messageDecrypted);

  const bobAddress = EthCrypto.publicKey.toAddress(bobPublicKey);
  console.log(bobAddress);
}

doEncrypt();