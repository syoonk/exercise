/*

  Contract for testing how to handle mutilple returned values in solidity.

*/

pragma solidity ^0.6.7;

contract ReturnVals {
    uint public returnedInt;
    string public returnedString;
    bool public returnedBool;
    
    function returnFunc(
        uint _intInput,
        string memory _stringInput,
        bool _boolInput)
    internal pure
    returns(
        uint _int,
        string memory _string,
        bool _bool) {
            _int = _intInput;
            _string = _stringInput;
            _bool = _boolInput;
        }
        
    function getFunc(uint _inputInt, string calldata _inputStr, bool _inputBool) 
    external {
        (returnedInt, returnedString, returnedBool) = returnFunc(_inputInt, _inputStr, _inputBool);
        // returnedInt = _returnedInt;
        // returnedString = _returnedString;
        // returnedBool = _returnedBool;
    }
}