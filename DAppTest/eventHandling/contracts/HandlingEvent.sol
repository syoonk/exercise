pragma solidity ^0.6.0;

contract HandlingEvent {
    event NewMessage(
        uint indexed eventId,
        address indexed from,
        address indexed to,
        string content);
        
    uint nextId;
        
    function sendMessage(address _to, string calldata _content)
    external {
        emit NewMessage(nextId, msg.sender, _to, _content);
        nextId++;
    }
}