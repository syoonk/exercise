pragma solidity 0.6.8;

contract EnumToInt {
    enum ToInt {
        ZERO,
        ONE,
        TWO,
        THREE
    }
    
    uint[] states = new uint[](4);
    
    // [0]: 3, [1]: 2, [2]: 1, [3] : 0
    function initializing()
    external {
        uint j = states.length - 1;
        for(uint i = 0; i < states.length; i++) {
            states[i] = j;
            j--;
        }
    }
    
    function putState(ToInt _states) 
    external {
        uint converted = uint(_states);
        states[converted] = converted;
        
    }
    
    function returnstates()
    external view
    returns(uint[] memory) {
        return states;
    }
    
}