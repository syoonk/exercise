/*
 *  To Check Caller Address And Modifier is properly work for it.
 *  Make trueOfFalse[contractB] true and expecting sender returnValue call from B
 *  is contract B's Address.
 *  Value should be (12345678910, contractBAddress)
 */
pragma solidity 0.6.8;

contract A {
    mapping(address => bool) trueOrFalse;
    uint returnVal = 12345678910;
    
    constructor()
    public {
        trueOrFalse[msg.sender] = true;
    }
    
    function returnPlease()
    public view
    returns(uint, address) {
        require(trueOrFalse[msg.sender],
            "not allowed");
        
        return (returnVal, msg.sender);
    }
    
    function makeAllow(address _address)
    public {
        trueOrFalse[_address] = !trueOrFalse[_address];
    }
}

contract B {
    A a;
    
    uint public returnVal;
    address public sender;
    
    constructor(address _aAddress)
    public {
        a = A(_aAddress);
    }
    
    function callA()
    external {
        (returnVal, sender) = a.returnPlease();
    }
    
    function initializing()
    external {
        returnVal = 0;
        sender = address(0);
    }
    
    function returning()
    external view 
    returns(uint, address) {
        return(returnVal, sender);
    }
}