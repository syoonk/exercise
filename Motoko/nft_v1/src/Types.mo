import Nat "mo:base/Nat";
import Nat8 "mo:base/Nat8";
import Nat16 "mo:base/Nat16";
import Nat32 "mo:base/Nat32";
import Nat64 "mo:base/Nat64";
import Blob "mo:base/Blob";
import Principal "mo:base/Principal";

module {
  public type Dip721NonFungibleToken = {
    logo: LogoResult;
    name: Text;
    symbol: Text;
    maxLimit: Nat16;
  };

  public type LogoResult = {
    logoType: Text;
    data: Text
  };

  public type TransactionId = Nat;
  public type TokenId = Nat64;

  public type Nft = {
    owner: Principal;
    id: TokenId;
    metadata: MetadataDesc;
  };

  public type MetadataDesc = [MetadataPart];

  public type MetadataPart = {
    purpose: MetadataPurpose;
    keyValData: [MetadataKeyVal];
    data: Blob;
  };

  public type MetadataPurpose = {
    #Preview;
    #Rendered;
  };

  public type MetadataKeyVal = {
    key: Text;
    val: MetadataVal;
  };

  public type MetadataVal = {
    #TextContent: Text;
    #BlobContent: Blob;
    #NatContent: Nat;
    #Nat8Content: Nat8;
    #Nat16Content: Nat16;
    #Nat32Content: Nat32;
    #Nat64Content: Nat64;
  };

  public type OwnerResult = Result<Principal, ApiError>;

  public type Result<S, E> = {
    #Ok: S;
    #Err: E;
  };

  public type ApiError = {
    #Unauthorized;
    #InvalidTokenId;
    #ZeroAddress;
    #Other;
  };

  public type TxReceipt = Result<TransactionId, ApiError>;

  public type InterfaceId = {
    #Approval;
    #TransactionHistory;
    #Mint;
    #Burn;
    #TransferNotification
  };

  public type MetadataResult = Result<MetadataDesc, ApiError>;

  public type ExtendedMetadataResult = Result<{ metadataDesc: MetadataDesc; tokenId: TokenId; }, ApiError>;

  public type MintReceipt = Result<{ tokenId: TokenId; id: Nat; }, ApiError>;
};