import Principal "mo:base/Principal";
import List "mo:base/List";
import Nat64 "mo:base/Nat64";

import Types "./Types";

shared actor class DIP721V1(custodian: Principal, init: Types.Dip721NonFungibleToken) {
  stable var transactionId: Types.TransactionId = 0;
  stable var nfts = List.nil<Types.Nft>();
  stable var custodians = List.make<Principal>(custodian);

  // Initiating NFT
  stable var logo: Types.LogoResult = init.logo;
  stable var name: Text = init.name;
  stable var symbol: Text = init.symbol;
  stable var maxLimit: Nat16 = init.maxLimit;

  let NULL_ADDRESS : Principal = Principal.fromText("aaaa-aa");

  public query func balanceOfDip721(_user: Principal): async Nat64 {
    return Nat64.fromNat(
      List.size(
        List.filter(nfts, func(_token: Types.Nft): Bool { _token.owner == _user })
      )
    );
  };

  public query func ownerOfDip712(_tokenId: Types.TokenId): async Types.OwnerResult {
    let item = List.find(nfts, func(_token: Types.Nft): Bool { _token.id == _tokenId });

    switch (item) {
      case (null) {
        return #Err(#InvalidTokenId);
      };
      case (?token) {
        return #Ok(token.owner);
      };
    };
  };

  public shared({ caller }) func safeTransferFromDip721(
    _from: Principal,
    _to: Principal,
    _tokenId: Types.TokenId
  ): async Types.TxReceipt {
    if (_to == NULL_ADDRESS) {
      return #Err(#ZeroAddress)
    } else {
      return transferFrom(_from, _to, _tokenId, caller);
    };
  };

  public shared({ caller }) func transferFromDip721(
    _from: Principal,
    _to: Principal,
    _tokenId: Types.TokenId
  ): async Types.TxReceipt {
    return transferFrom(_from, _to, _tokenId, caller);
  };

  public shared({ caller }) func mintDip721(_to: Principal, _metadata: Types.MetadataDesc): async Types.MintReceipt {
    if (not List.some(custodians, func (_custodian: Principal): Bool { _custodian == caller })) {
      return #Err(#Unauthorized);
    };

    let newId = Nat64.fromNat(List.size(nfts));
    let nft: Types.Nft = {
      owner = _to;
      id = newId;
      metadata = _metadata
    };

    nfts := List.push(nft, nfts);

    transactionId += 1;

    return #Ok({
      tokenId = newId;
      id = transactionId;
    });
  };

  public query func supportedInterfacesDip721(): async [Types.InterfaceId] {
    return [#TransferNotification, #Burn, #Mint];
  };

  public query func logoDip721(): async Types.LogoResult {
    return logo;
  };

  public query func nameDip721(): async Text {
    return name;
  };

  public query func symbolDip721(): async Text {
    return symbol;
  };

  public query func totalSupplyDip721(): async Nat64 {
    return Nat64.fromNat(List.size(nfts));
  };

  public query func getMetadataDip721(_tokenId: Types.TokenId): async Types.MetadataResult {
    let item = List.find(nfts, func(_token: Types.Nft): Bool { _token.id == _tokenId });

    switch (item) {
      case null {
        return #Err(#InvalidTokenId);
      };
      case (?token) {
        return #Ok(token.metadata);
      };
    };
  };

  public query func getMetadataForUserDip721(_user: Principal): async Types.ExtendedMetadataResult {
    let item = List.find(nfts, func(_token: Types.Nft): Bool { _token.owner == _user });

    switch (item) {
      case null {
        return #Err(#Other);
      };
      case (?token) {
        return #Ok({
          metadataDesc = token.metadata;
          tokenId = token.id;
        });
      };
    };
  };

  public query func getTokenIdsForUserDip721(_user: Principal): async [Types.TokenId] {
    let items = List.filter(nfts, func(_token: Types.Nft): Bool { _token.owner == _user });
    let tokenIds = List.map(items, func(_item: Types.Nft): Types.TokenId { _item.id });

    return List.toArray(tokenIds);
  };

  public query func getMaxLimitDip721(): async Nat16 {
    return maxLimit;
  };

  func transferFrom(
    _from: Principal,
    _to: Principal,
    _tokenId: Types.TokenId,
    _caller: Principal
  ): Types.TxReceipt {
    let item = List.find(nfts, func(_token: Types.Nft): Bool { _token.id == _tokenId });

    switch (item) {
      case null {
        return #Err(#InvalidTokenId);
      };
      case(?token) {
        if (
          _caller != token.owner and
          not List.some(custodians, func(custodian: Principal): Bool { custodian == _caller })
        ) {
          return #Err(#Unauthorized);
        } else if (Principal.notEqual(_from, token.owner)) {
          return #Err(#Other);
        } else {
          nfts := List.map(nfts, func (_item: Types.Nft): Types.Nft {
            if (_item.id == token.id) {
              let update: Types.Nft = {
                owner = _to;
                id = _item.id;
                metadata = token.metadata;
              };

              return update;
            } else {
              return _item;
            };
          });

          transactionId += 1;

          return #Ok(transactionId);
        }
      }
    }
  };

}