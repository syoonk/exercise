# Deploy
```shell
dfx start

dfx deploy
```

# Upgrade
Just deploy. It may check itself its upgadeability.

# Call
```
dfx canister call hello_world greet Kong

=> ("Hello, Kong!")
```