import Debug "mo:base/Debug"

actor HelloWorld {

  public func greet(name: Text): async Text {
    Debug.print("Hello World Debug");

    return "Hello, " # name # "!";
  }

}