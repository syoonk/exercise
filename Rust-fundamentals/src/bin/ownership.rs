// Topic: Ownership
//
// Requirements:
// * Print out the quantity and id number of a grocery item

struct Item {
  id: i32,
  quantity: i32,
}

fn display_id(_item: &Item) {
  println!("ID: {:?}", _item.id);
}

fn display_quantity(_item: &Item) {
  println!("Quantity: {:?}", _item.quantity);
}

fn main() {
  let item = Item { id: 1, quantity: 10 };

  display_id(&item);
  display_quantity(&item);
}
