// Topic: Data management using tuples
//
// Requirements:
// * Print whether the y-value of a cartesian coordinate is
//   greater than 5, less than 5, or equal to 5

fn coordinates() -> (i16, i16) {
  (1, 5)
}

fn main() {
  let (x, y) = coordinates();

  if y == 5 {
    println!("=5");
  } else if y < 5 {
    println!("<5");
  } else {
    println!(">5");
  }
}
