// Topic: Iterator
//
// Requirements:
// * Triple the value of each item in a vector.
// * Filter the data to only include values > 10.
// * Print out each element using a for loop.

fn main() {
  let data = vec![1, 2, 3, 4, 5];

  let result: Vec<i32> = data
    .iter()
    .map(|_num| _num * 3)
    .filter(|_num| _num > &10)
    .collect();

  for _result in result {
    println!("{:?}", _result);
  }
}
