// Topic: Functions
//
// Program requirements:
// * Displays your first and last name

fn first_name() {
  println!("Sungyoon");
}

fn last_name() {
  println!("Kong");
}

fn main() {
  first_name(); last_name();
}
