// Topic: Option
//
// Requirements:
// * Print out the details of a student's locker assignment
// * Lockers use numbers and are optional for students

struct Student {
  name: String,
  locker: Option<i16>
}

fn student_assignment() -> Vec<Student> {
  let students: Vec<Student> = vec![
    Student { name: "A".to_owned(), locker: Some(1) },
    Student { name: "B".to_owned(), locker: Some(12) },
    Student { name: "C".to_owned(), locker: None }
  ];

  return students;
}

fn main() {
  let students = student_assignment();

  for _student in students {
    match _student.locker {
      Some(num) => println!("{:?}'s locker is {:?}", _student.name, num),
      None => println!("{:?} has no locker", _student.name)
    }
  }
}
