// Topic: Advanced match
//
// Requirements:
// * Print out a list of tickets and their information for an event
// * Tickets can be Backstage, Vip, and Standard
// * Backstage and Vip tickets include the ticket holder's name
// * All tickets include the price

enum Ticket {
  Backstage(String, f64),
  Standard(f64),
  Vip(String, f64)
}

fn main() {
  let tickets = vec![
    Ticket::Backstage(String::from("Sungyoon"), 4000.0),
    Ticket::Vip("syoonk".to_owned(), 40000.0),
    Ticket::Standard(10000.0)
  ];

  for _ticket in tickets {
    match _ticket {
      Ticket::Backstage(holder, price) => println!("Backstage ticket:: holder: {:?}, price: {:?}", holder, price),
      Ticket::Standard(price) => println!("Standard ticket:: price: {:?}", price),
      Ticket::Vip(holder, price) => println!("Vip ticket:: holder: {:?}, price: {:?}", holder, price),
    }
  }
}
