// Topic: Traits
//
// Requirements:
// * Calculate the perimeter of a square and triangle:
//   * The perimeter of a square is the length of any side*4.
//   * The perimeter of a triangle is a+b+c where each variable
//     represents the length of a side.
// * Print out the perimeter of the shapes

use rand::Rng;

trait Perimeter {
  fn perimeter_calculator(&self) -> u32;
}

struct Square {
  horizontal_upper: u32,
  horizontal_lower: u32,
  vertical_left: u32,
  vertical_right: u32
}

impl Square {
  fn new() -> Self {
    let mut rng = rand::thread_rng();

    return Self {
      horizontal_upper: rng.gen_range(1..=100) as u32,
      horizontal_lower: rng.gen_range(1..=100) as u32,
      vertical_left: rng.gen_range(1..=100) as u32,
      vertical_right: rng.gen_range(1..=100) as u32
    };
  }

  fn show_sides(&self) {
    println!("horizontal_upper: {:?}", self.horizontal_upper);
    println!("horizontal_lower: {:?}", self.horizontal_lower);
    println!("vertical_left: {:?}", self.vertical_left);
    println!("vertical_right: {:?}", self.vertical_right);
  }
}

impl Perimeter for Square {
  fn perimeter_calculator(&self) -> u32 {
    let mut sum: u32 = 0;

    sum += self.horizontal_upper;
    sum += self.horizontal_lower;
    sum += self.vertical_right;
    sum += self.vertical_left;

    return sum;
  }

}

struct Triangle {
  side1: u32,
  side2: u32,
  side3: u32
}

impl Triangle {
  fn new() -> Self {
    let mut rng = rand::thread_rng();

    return Self {
      side1: rng.gen_range(1..=100),
      side2: rng.gen_range(1..=100),
      side3: rng.gen_range(1..=100)
    };
  }

  fn show_sides(&self) {
    println!("side1: {:?}", self.side1);
    println!("side2: {:?}", self.side2);
    println!("side3: {:?}", self.side3);
  }
}

impl Perimeter for Triangle {
  fn perimeter_calculator(&self) -> u32 {
    let mut sum: u32 = 0;

    sum += self.side1;
    sum += self.side2;
    sum += self.side3;

    return sum;
  }
}

fn get_perimeter(shape: impl Perimeter) -> u32 {
  return shape.perimeter_calculator();
}

fn main() {
  let square = Square::new();
  println!("Square:: ");
  square.show_sides();
  println!("Perimeter: {:?}", get_perimeter(square));

  println!("   ");

  let triangle = Triangle::new();
  println!("Triangle:: ");
  triangle.show_sides();
  println!("Perimeter: {:?}", get_perimeter(triangle));

}
