// Topic: Map combinator
//
// Requirements:
// * Given a user name, create and print out a User struct if the user exists

use std::io;

#[derive(Debug)]
struct User {
    user_id: i32,
    name: String,
}

/// Locates a user id based on the name.
fn find_user(name: &str) -> Option<i32> {
    let name = name.to_lowercase();
    match name.as_str() {
        "sam" => Some(1),
        "matt" => Some(5),
        "katie" => Some(9),
        _ => None,
    }
}

fn main() {
    let mut buffer = String::new();
    let mut user_name = String::new();

    println!("Enter name:");
    loop {
        let input_status = io::stdin().read_line(&mut buffer);

        if input_status.is_ok() {
            user_name = buffer.trim().to_owned();

            if &user_name == "" {
            } else {
                user_name = user_name.to_lowercase();

                break;
            }
        }
    }

    let user = find_user(user_name.as_str())
        .map(|user_id| User { user_id, name: user_name });

    match user {
        Some(user) => println!("{:?}", user),
        None => println!("user not found")
    }
}
