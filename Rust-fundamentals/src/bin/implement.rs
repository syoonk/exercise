// Topic: Implementing functionality with the impl keyword
//
// Requirements:
// * Print the characteristics of a shipping box
// * Must include dimensions, weight, and color

enum Color {
  Red,
  Green,
  Blue,
  White,
  Black
}

impl Color {
  fn print(&self) {
    match self {
      Color::Red => println!("Color: Red"),
      Color::Green => println!("Color: Green"),
      Color::Blue => println!("Color: Blue"),
      Color::White => println!("Color: White"),
      Color::Black => println!("Color: Black"),
    }
  }
}

struct Dimensions {
  width: f64,
  height: f64,
  depth: f64
}

impl Dimensions {
  fn print(&self) {
    println!("Width: {:?}, Height: {:?}, Depth: {:?}", self.width, self.height, self.depth);
  }
}

struct ShippingBox {
  dimensions: Dimensions,
  weight: f64,
  color: Color
}

impl ShippingBox {
  fn create_new_box(
    dimensions: Dimensions,
    weight: f64,
    color: Color
  ) -> Self {
    return Self { dimensions, weight, color };
  }

  fn print_characteristics(&self) {
    self.dimensions.print();
    println!("weight: {:?}", self.weight);
    self.color.print();
  }
}

fn main() {
  let new_box = ShippingBox::create_new_box(
    Dimensions {
      width: 3.0,
      height: 4.0,
      depth: 5.0
    },
    18.5,
    Color::Black
  );

  new_box.print_characteristics();
}
