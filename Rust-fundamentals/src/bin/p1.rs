// Project 1: Interactive bill manager
//
// User stories:
// * L1: I want to add bills, including the name and amount owed.
// * L1: I want to view existing bills.
// * L2: I want to remove bills.
// * L3: I want to edit existing bills.
// * L3: I want to go back if I change my mind.
//
// Tips:
// * Use the loop keyword to create an interactive menu.
// * Each menu choice should be it's own function, so you can work on the
//   the functionality for that menu in isolation.
// * A vector is the easiest way to store the bills at level 1, but a
//   hashmap will be easier to work with at levels 2 and 3.
// * Create a function just for retrieving user input, and reuse it
//   throughout your program.
// * Create your program starting at level 1. Once finished, advance to the
//   next level.

use std::io;
use std::collections::HashMap;

struct Bill {
  name: String,
  amount: f64
}

struct Bills {
  inner: HashMap<String, Bill>
}

impl Bills {
  fn new() -> Self {
    Self {
      inner: HashMap::new()
    }
  }

  fn add(&mut self, bill: Bill) {
    self.inner.insert(bill.name.clone(), bill);
  }

  fn get_all(&self) -> Vec<&Bill> {
    let mut bills = vec![];

    for bill in self.inner.values() {
      bills.push(bill);
    }

    return bills;
  }

  fn remove(&mut self, name: &str) -> bool {
    return self.inner.remove(name).is_some();
  }

  fn update(&mut self, name: &str, amount: f64) -> bool {
    match self.inner.get_mut(name) {
      Some(bill) => {
        bill.amount = amount;

        return true;
      }
      None => return false
    }
  }
}

fn get_input() -> Option<String> {
  let mut buffer = String::new();

  while io::stdin().read_line(&mut buffer).is_err() {
    println!("Please enter your data again");
  }

  let input = buffer.trim().to_owned();
  if &input == "" {
    None
  } else {
    Some(input)
  }
}

fn add_bill_menu(bills: &mut Bills) {
  println!("Bill name:");

  let name = match get_input() {
    Some(input) => input,
    None => return
  };

  let amount = match get_bill_amount() {
    Some(amount) => amount,
    None => return
  };

  let bill = Bill { name, amount };
  bills.add(bill);

  println!("Bill added");
}

fn remove_bill_menu(bills: &mut Bills) {
  let mut i = 1;

  println!("# bill list");
  for _bill in bills.get_all() {
    println!("{:?}: {:?}", i, _bill.name);

    i += 1;
  }

  println!("Enter bill name to remove:");
  let name = match get_input() {
    Some(name) => name,
    None => return
  };

  if bills.remove(&name) {
    println!("{:?} has been removed", name);
  } else {
    println!("{:?} is not found", name);
  }
}

fn update_bill_menu(bills: &mut Bills) {
  let mut i = 1;

  println!("# bill list");
  for _bill in bills.get_all() {
    println!("{:?}: {:?}; {:?}", i, _bill.name, _bill.amount);

    i += 1;
  }

  println!("Enter bill to update:");
  let name = match get_input() {
    Some(name) => name,
    None => return
  };

  let amount = match get_bill_amount() {
    Some(amount) => amount,
    None => return
  };

  if bills.update(&name, amount) {
    println!("{:?} is updated to {:?}", name, amount);
  } else {
    println!("{:?} is not found", name);
  }
}

fn view_bills_menu(bills: &Bills) {
  for _bill in bills.get_all() {
    println!("{:?}: {:?}", _bill.name, _bill.amount);
  }
}

fn get_bill_amount() -> Option<f64> {
  println!("Amount: ");
  loop {
    let input = match get_input() {
      Some(input) => input,
      None => return None,
    };

    if &input == "" {
      return None;
    }

    let parsed_input: Result<f64, _> = input.parse();
    match parsed_input {
      Ok(amount) => return Some(amount),
      Err(_) => println!("Please enter a number")
    }

  }
}

fn show_options() {
  println!("");
  println!("1. Add bill");
  println!("2. View bills");
  println!("3. Remove bill");
  println!("4. Update bill");
  println!("");
  println!("Enter selection:");
}

fn menu_selector(bills: &mut Bills) {

  loop {
    show_options();

    let input = match get_input() {
      Some(input) => input,
      None => return
    };

    match input.as_str() {
      "1" => add_bill_menu(bills),
      "2" => view_bills_menu(bills),
      "3" => remove_bill_menu(bills),
      "4" => update_bill_menu(bills),
      _ => println!("Invalid option")
    }
  }
}

fn main() {
  let mut bills = Bills::new();

  // Adding sample bills
  bills.add(Bill { name: "Sungyoon".to_owned(), amount: 1000.00 });
  bills.add(Bill { name: "Kong".to_owned(), amount: 2200.00 });
  bills.add(Bill { name: "Syoonk".to_owned(), amount: 3230.034 });
  bills.add(Bill { name: "Babo".to_owned(), amount: 1123.1 });

  menu_selector(&mut bills);
}
