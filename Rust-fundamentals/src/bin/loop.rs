// Topic: Looping using the loop statement
//
// Program requirements:
// * Display "1" through "4" in the terminal

fn main() {
  let mut i = 1 as i32;

  loop {
    println!("{:?}", i);

    if i == 4 {
      break;
    }

    i = i + 1;
  }

  println!("Done.");
}
