// Topic: Working with expressions
//
// Requirements:
// * Print "its big" if a variable is > 100
// * Print "its small" if a variable is <= 100

fn print_message(_compared: bool) {
  match _compared {
    true => println!("its big"),
    false => println!("its small")
  }
}

fn main() {
  let value = 101;
  let compared_100 = value > 100;

  print_message(compared_100);
}
