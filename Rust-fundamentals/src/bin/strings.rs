// Topic: Strings
//
// Requirements:
// * Print out the name and favorite colors of people aged 10 and under

struct Person {
  name: String,
  age: i16,
  favorite_color: String
}

fn making_people() -> Vec<Person> {
  let mut people: Vec<Person> = Vec::new();

  people.push(
    Person {
      name: "Sungyoon".to_owned(),
      age: 18,
      favorite_color: "pink".to_owned()
    }
  );

  people.push(
    Person {
      name: "syoonk".to_owned(),
      age: 27,
      favorite_color: "purple".to_owned()
    }
  );

  people.push(
    Person {
      name: String::from("Kong"),
      age: 34,
      favorite_color: String::from("Yellow")
    }
  );

  return people;
}

fn print(_to_print: &str) {
  println!("{:?}", _to_print);
}

fn main() {
  let people: Vec<Person> = making_people();

  for _person in &people {
    if _person.age < 34 {
      print(&_person.name);
      print(&_person.favorite_color);
    }
  }
}
