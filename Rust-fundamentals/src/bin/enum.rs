// Topic: Working with an enum
//
// Program requirements:
// * Prints the name of a color to the terminal

enum Color {
  Red,
  Blue,
  Green,
  Black,
  White
}

fn print_color(_color: Color) {
  match _color {
    Color::Red => println!("Red"),
    Color::Blue => println!("Blue"),
    Color::Green => println!("Green"),
    Color::Black => println!("Black"),
    Color::White => println!("White"),
  }
}

fn main() {
  print_color(Color::Red);
  print_color(Color::Blue);
  print_color(Color::Green);
  print_color(Color::Black);
  // print_color(Color::White);
}
