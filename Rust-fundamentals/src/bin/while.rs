// Topic: Looping using the while statement
//
// Program requirements:
// * Counts down from 5 to 1, displays the countdown
//   in the terminal, then prints "done!" when complete.

fn main() {
  let mut i = 5 as i8;

  while i > 0 {
    println!("{:?}", i);

    i = i - 1;
  }

  println!("Done.");
}
