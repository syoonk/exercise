// Topic: Decision making with match
//
// Program requirements:
// * Display "one", "two", "three", or "other" based on whether
//   the value of a variable is 1, 2, 3, or some other number,
//   respectively

fn main() {
  let val = 2;

  match val {
    1 => { println!("One"); },
    2 => { println!("Two"); },
    3 => { println!("Three"); },
    _ => { println!("Other numbers "); }
  }
}
