// Topic: Basic arithmetic
//
// Program requirements:
// * Displays the result of the sum of two numbers

fn add(x: i32, y: i32) -> i32 {
  return x + y;
}

fn main() {
  println!("Adding result: {:?}", add(3, 5));
}
