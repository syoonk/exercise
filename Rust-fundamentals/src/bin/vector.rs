// Topic: Vectors
//
// Requirements:
// * Print 10, 20, "thirty", and 40 in a loop
// * Print the total number of elements in a vector

fn main() {
  let mut numbers: Vec<i32> = Vec::new();
  numbers.push(10);
  numbers.push(20);
  numbers.push(30);
  numbers.push(40);

  for _number in &numbers {
    if _number == &30 {
      println!("thirty");
    } else {
      println!("{:?}", _number);
    }
  }

  println!("Total number of elements: {:?}", numbers.len());
}
