// Project 2: Contact manager
//
// User stories:
// * L1: I want to view my saved contacts.
// * L2: I want to add new contacts.
// * L2: I want to search for contacts.
// * L3: I want to edit and remove existing contacts.
//
// Tips:
// * Make a backup of the existing `p2_data.csv` file.
// * CSV files contain records and fields:
//   Each line is a "record" which contain "fields" of information.
//   The fields are separated by commas. If a field is not provided,
//   then there is no data for that particular field. There will
//   always be a comma for the field even if no data is present.
// * The `id` and `name` fields are required, the `email` field is optional.
// * Check the documentation on the `std::fs::File` struct for reading
//   and writing files.
// * Use the `split` function from the standard library to extract
//   specific fields.
// * Try the `structopt` crate if you want to make a non-interactive
//   command line application.
// * Create your program starting at level 1. Once finished, advance
//   to the next level.
// * Make your program robust: there are 7 errors & multiple blank lines
//   present in the data.

use std::collections::HashMap;
use thiserror::Error;
use std::fs::{File, OpenOptions};
use std::path::PathBuf;
use std::io::{Read, Write};
use structopt::StructOpt;

#[derive(Debug)]
struct Record {
  id: u32,
  name: String,
  email: Option<String>
}

struct Records {
  inner: HashMap<u32, Record>
}

impl Records {
  fn new() -> Self {
    Self {
      inner: HashMap::new()
    }
  }

  fn add(&mut self, _record: Record) {
    self.inner.insert(_record.id, _record);
  }

  fn into_vec(mut self) -> Vec<Record> {
    let mut records: Vec<_> = self.inner.drain().map(|kv| kv.1).collect();
    records.sort_by_key(|rec| rec.id);
    records
  }

  fn next_id(&self) -> u32 {
    let mut ids: Vec<_> = self.inner.keys().collect();
    ids.sort();

    match ids.pop() {
      Some(id) => id + 1,
      None => 1
    }
  }

  fn search(&self, name: &str) -> Vec<&Record> {
    self.inner
      .values()
      .filter(|rec| rec.name.to_lowercase().contains(&name.to_lowercase()))
      .collect()
  }

  fn remove(&mut self, id: u32) -> Option<Record> {
    self.inner.remove(&id)
  }

  fn edit(
    &mut self,
    id: u32,
    name: &str,
    email: Option<String>
  ) {
    self.inner.insert(id, Record { id, name: name.to_string(), email });
  }
}

#[derive(Error, Debug)]
enum ParseError {
  #[error("id must be a unsigned number: {0}")]
  InvalidId(#[from] std::num::ParseIntError),

  #[error("empty record")]
  EmptyRecord,

  #[error("missing field: {0}")]
  MissingField(String)
}

fn parse_record(record: &str) -> Result<Record, ParseError> {
  let fields: Vec<&str> = record.split(',').collect();

  let id = match fields.get(0) {
    Some(id) => u32::from_str_radix(id, 10)?,
    None => return Err(ParseError::EmptyRecord)
  };

  let name = match fields.get(1).filter(|name| **name != "") {
    Some(name) => name.to_string(),
    None => return Err(ParseError::MissingField("name".to_owned()))
  };

  let email = fields.get(2).map(|email| email.to_string()).filter(|email| email != "");

  Ok(Record { id, name, email })
}

fn parse_records(_records: String, _verbose: bool) -> Records {
  let mut records = Records::new();
  for (_num, _record) in _records.split("\n").enumerate() {
    if _record != "" {
      match parse_record(_record) {
        Ok(rec) => records.add(rec),
        Err(e) => {
          if _verbose {
            println!(
              "error on line number {}: {}\n > \"{}\"\n",
              _num + 1,
              e,
              _record
            );
          }
        }
      }
    }
  }

  records
}

fn load_records(_file_name: PathBuf, _verbose: bool) -> std::io::Result<Records> {
  let mut file = File::open(_file_name)?;

  let mut buffer = String::new();
  file.read_to_string(&mut buffer)?;

  Ok(parse_records(buffer, _verbose))
}

fn save_records(file_name: PathBuf, records: Records) -> std::io::Result<()> {
  let mut file = OpenOptions::new()
    .write(true)
    .truncate(true)
    .open(file_name)?;

  file.write(b"id,name,email\n")?;

  for record in records.into_vec().into_iter() {
    let email = match record.email {
      Some(email) => email,
      None => "".to_string()
    };

    let line = format!("{},{},{}\n", record.id, record.name, email);

    file.write(line.as_bytes())?;
  }

  file.flush()?;

  Ok(())
}

#[derive(StructOpt, Debug)]
#[structopt(about = "Project 2: contact manager")]
struct Opt {
  #[structopt(short, parse(from_os_str), default_value = "p2_data.csv")]
  data_file: PathBuf,
  #[structopt(subcommand)]
  cmd: Command,
  #[structopt(short, help = "verbose")]
  verbose: bool
}

#[derive(StructOpt, Debug)]
enum Command {
  List {},

  Add {
    name: String,
    #[structopt(short)]
    email: Option<String>
  },

  Search {
    query: String
  },

  Remove {
    id: u32
  },

  Edit {
    id: u32,
    name: String,
    #[structopt(short)]
    email: Option<String>
  }
}

fn run(opt: Opt) -> Result<(), std::io::Error> {
  match opt.cmd {
    Command::List { .. } => {
      let records = load_records(opt.data_file, opt.verbose)?;
      for record in records.into_vec().into_iter() {
        println!("{:?}", record);
      }
    },

    Command::Add { name, email } => {
      let mut recs = load_records(opt.data_file.clone(), opt.verbose)?;

      recs.add(Record {
        id: recs.next_id() as u32,
        name,
        email
      });
      save_records(opt.data_file, recs)?;
    },

    Command::Search { query } => {
      let recs = load_records(opt.data_file, opt.verbose)?;

      let results = recs.search(&query);
      if results.is_empty() {
        println!("no records found");
      } else {
        for rec in results {
          println!("{:?}", rec);
        }
      }
    },

    Command::Remove { id } => {
      let mut recs = load_records(opt.data_file.clone(), opt.verbose)?;

      if recs.remove(id).is_some() {
        save_records(opt.data_file, recs)?;

        println!("record deleted");
      } else {
        println!("record not found");
      }
    },

    Command::Edit { id, name, email } => {
      let mut recs = load_records(opt.data_file.clone(), opt.verbose)?;

      recs.edit(id, &name, email);
      save_records(opt.data_file, recs)?;
    }
  }

  Ok(())
}

fn main() {
  let opt = Opt::from_args();

  if let Err(e) = run(opt) {
    println!("an error occured: {}", e);
  }
}
