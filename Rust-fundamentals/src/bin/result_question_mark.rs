// Topic: Result & the question mark operator
//
// Requirements:
// * Determine if an employee can access a building using a digital keycard
// * Employees that can access the building are:
//   * Maintenance crews
//   * Marketing department employees
//   * Managers
// * Other employees that work at the company are:
//   * Line supervisors
//   * Kitchen staff
//   * Assembly technicians
// * Ensure that terminated employees cannot access the building
//   regardless of their position

use std::env;
use rand::Rng;

enum Position {
  MaintenanceCrew,
  Marketing,
  Manager,
  LineSupervisor,
  KitchenStaff,
  AssemblyTechnician
}

impl Position {
  fn position_matcher(index: u8) -> Position {
    match index {
      0 => return Position::MaintenanceCrew,
      1 => return Position::Marketing,
      2 => return Position::Manager,
      3 => return Position::LineSupervisor,
      4 => return Position::KitchenStaff,
      5 => return Position::AssemblyTechnician,
      _ => panic!("Invalid position index")
    }
  }

  fn access(&self) -> bool {
    match self {
      Position::MaintenanceCrew => return true,
      Position::Marketing => return true,
      Position::Manager => return true,
      _ => return false
    }
  }

  fn print(&self) {
    match self {
      Position::MaintenanceCrew => println!("Maintenance Crew"),
      Position::Marketing => println!("Marketing"),
      Position::Manager => println!("Manager"),
      Position::LineSupervisor => println!("Line Supervisor"),
      Position::KitchenStaff => println!("Kitchen Staff"),
      Position::AssemblyTechnician => println!("Assembly Technician")
    }
  }
}

struct Employee {
  position: Position,
  active: bool
}

fn listing_employees(num_of_employees: u8) -> Vec<Employee> {
  let mut employees: Vec<Employee> = Vec::new();

  let mut rng = rand::thread_rng();
  for _i in 0..num_of_employees {
    let position = Position::position_matcher(rng.gen_range(0..6) as u8);
    let active = if rng.gen_range(0..10) < 8 { true } else { false };

    employees.push(Employee { position, active } );
  }

  return employees;
}

fn access_checker(employee: &Employee) -> Result<(), String> {
  let position_access = Position::access(&employee.position);

  if position_access && employee.active {
    Ok(())
  } else if position_access && !employee.active {
    Err("Employee is not active".to_owned())
  } else if !position_access && employee.active {
    Err("Employee is not on proper position".to_owned())
  } else {
    Err("Employee is not active and not on proper position".to_owned())
  }
}

fn main() {
  let args: Vec<String> = env::args().collect();
  let number_of_employees = (args[1].to_owned()).parse::<u8>().unwrap();

  let employees = listing_employees(number_of_employees);

  fn print_access(employee: &Employee) -> Result<(), String> {
    let attempt_access = access_checker(employee)?;
    println!("access granted");
    Ok(())
  }

  for i in 0..employees.len() {
    println!("{:?} -> position: {:?}, active: {:?}", i, Position::print(&employees[i].position), employees[i].active);

    match print_access(&employees[i]) {
      Err(e) => println!("access denied: {:?}", e),
      _ => ()
    }

    println!();
  }
}
