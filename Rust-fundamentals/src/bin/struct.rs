// Topic: Organizing similar data using structs
//
// Requirements:
// * Print the flavor of a drink and it's fluid ounces

enum Flavor {
  Grape,
  Orange,
  Lemon,
  Blueberry
}

struct Drink {
  volume: f32,
  flavor: Flavor
}

fn print_flavor(_drink: Drink) {
  match _drink.flavor {
    Flavor::Grape => println!("flavor: grape"),
    Flavor::Orange => println!("flavor: orange"),
    Flavor::Lemon => println!("flavor: lemon"),
    Flavor::Blueberry => println!("flavor: blueberry"),
  }

  println!("volume: {:?}", _drink.volume);
}

fn main() {
  let drink_lemon_200_20 = Drink {
    flavor: Flavor::Lemon,
    volume: 200.20
  };

  print_flavor(drink_lemon_200_20);
}
