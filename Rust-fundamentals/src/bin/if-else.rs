// Topic: Flow control using if..else
//
// Program requirements:
// * Displays a message based on the value of a boolean variable
// * When the variable is set to true, display "hello"
// * When the variable is set to false, display "goodbye"

fn main() {
  let val = false;

  if val {
    println!("hello");
  } else {
    println!("goodbye");
  }
}
