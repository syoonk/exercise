// Topic: Result
//
// Requirements:
// * Determine if a customer is able to make a restricted purchase
// * Restricted purchases require that the age of the customer
//   is at least 21

use rand::Rng;

struct Customer {
  age: i8
}

fn try_purchase(customer: &Customer) -> Result<(), String> {
  if customer.age < 21 {
    Err("customer must be at least 21 years old".to_owned())
  } else {
    Ok(())
  }
}

fn create_customers(_num_of_customers: i8) -> Vec<Customer> {
  let mut customers: Vec<Customer> = Vec::new();

  let mut rng = rand::thread_rng();
  for _x in 0.._num_of_customers {
    customers.push(Customer { age: rng.gen_range(1..100) as i8 });
  }

  return customers;
}


fn main() {
  let customers: Vec<Customer> = create_customers(10 as i8);

  for _customer in customers {
    println!("Customer age is {:?}... ", _customer.age);

    let result = try_purchase(&_customer);

    match result {
      Ok(_) => println!("Passed."),
      Err(e) => println!("Rejected: {:?}", e)
    }
  }
}
