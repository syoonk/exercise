// Topic: HashMap
//
// Requirements:
// * Print the name and number of items in stock for a furniture store
// * If the number of items is 0, print "out of stock" instead of 0
// * The store has:
//   * 5 Chairs
//   * 3 Beds
//   * 2 Tables
//   * 0 Couches
// * Print the total number of items in stock

use std::collections::HashMap;

fn listing_furnitures() -> HashMap<String, i32> {
  let mut furnitures: HashMap<String, i32> = HashMap::new();

  furnitures.insert("Chairs".to_owned(), 5);
  furnitures.insert("Beds".to_owned(), 3);
  furnitures.insert("Tables".to_owned(), 2);
  furnitures.insert("Couches".to_owned(), 0);

  return furnitures;
}

fn checking_stocks(furnitures: HashMap<String, i32>) {
  let mut total_stock = 0;

  for (item, qty) in furnitures {
    total_stock = total_stock + qty;

    let stock_count = if qty == 0 {
      "out of stock".to_owned()
    } else {
      format!("{:?}", qty)
    };

    println!("item={:?}, stock={:?}", item, stock_count);
  }

  println!("total stock={:?}", total_stock);
}

fn main() {
  let furnitures = listing_furnitures();

  checking_stocks(furnitures);
}
