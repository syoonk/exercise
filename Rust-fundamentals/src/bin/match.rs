// Topic: Decision making with match
//
// Program requirements:
// * Display "it's true" or "it's false" based on the value of a variable

fn main() {
  let val = true;

  match val {
    true => { println!("It's true"); },
    false => { println!("It's false"); },
  }
}
