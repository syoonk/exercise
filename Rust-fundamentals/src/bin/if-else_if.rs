// Topic: Flow control using if..else if..else
//
// Program requirements:
// * Display ">5", "<5", or "=5" based on the value of a variable
//   is > 5, < 5, or == 5, respectively

fn main() {
  let val = 6;

  if val > 5 {
    println!(">5");
  } else if val < 5 {
    println!("<5");
  } else {
    println!("=5");
  }
}
