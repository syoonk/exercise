const app = Vue.createApp({
  data() {
    return {
      url: "http://www.naver.com",
      showBooks: true,
      books: [
        { title: 'name of the wind', author: 'patrick rothfuss' },
        { title: 'the way of kings', author: 'brandon sanderson' },
        { title: 'the final empire', author: 'brandon sanderson' },
      ],
      coins: [
        { symbol: 'adai', img: 'assets/adai.png', isFav: true },
        { symbol: 'air', img: 'assets/air.png', isFav: false },
        { symbol: 'eth', img: 'assets/eth.png', isFav: true },
      ]
    }
  },
  methods: {
    toggleShowBooks() {
      this.showBooks = !this.showBooks;
    }
  },
  computed: {
    filteredCoins() {
      return this.coins.filter(_el => _el.isFav);
    }
  }
});

app.mount('#app');