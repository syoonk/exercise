import Test from 0x01

transaction {

    prepare(minter: AuthAccount) {
        minter.link<&Test.Minter>(/private/Minter, target: /storage/Minter)

        log("Linked")
    }

}