import Test from 0x01

transaction {

    prepare(minter: AuthAccount, receiver: AuthAccount) {
        let minterCap = minter.getCapability<&Test.Minter>(/private/Minter)

        let minterProxy <- Test.createMinterProxy(cap: minterCap)
        receiver.save(<- minterProxy, to: /storage/MinterProxy)

        log("Saved minter proxy to the receiver")
    }

}