import Test from 0x01

transaction {

    prepare(minterProxy: AuthAccount) {
        let minterRef = minterProxy.borrow<&Test.MinterProxy>(from: /storage/MinterProxy)
            ?? panic("Cannot borrow minter proxy from storage")

        let nft <- minterRef.mintNft()

        log(nft.id)

        destroy nft
    }

}