pub contract Test {

    pub resource NFT {
        pub let id: UInt64

        init() {
            self.id = self.uuid
        }
    }

    pub resource Minter {
        pub fun mintNft(): @NFT {
            return <- create NFT()
        }
    }

    pub resource MinterProxy {
        pub let cap: Capability<&Minter>

        pub fun mintNft(): @NFT {
            let minterRef = self.cap.borrow()
                ?? panic("The Minter resource owner unlinked it from their private path")

            return <- minterRef.mintNft()
        }

        init(cap: Capability<&Minter>) {
            self.cap = cap
        }
    }

    pub fun createMinterProxy(cap: Capability<&Minter>): @MinterProxy {
        return <- create MinterProxy(cap: cap)
    }

    init() {
        self.account.save(<- create Minter(), to: /storage/Minter)
        self.account.link<&Minter>(/private/Minter, target: /storage/Minter)
    }

}