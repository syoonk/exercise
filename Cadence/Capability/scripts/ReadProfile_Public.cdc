import Example from 0x01

pub fun main(account: Address): String {
  let profile = getAccount(account).getCapability(/public/Profile)
    .borrow<&Example.Profile{Example.ProfilePublic}>()
    ?? panic("Could not borrow the public Profile")

  return profile.name
}