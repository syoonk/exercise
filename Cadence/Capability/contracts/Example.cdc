pub contract Example {

    pub resource interface ProfilePublic {
        pub let name: String
    }

    pub resource Profile: ProfilePublic {
        pub let name: String
        pub let email: String

        init(name: String, email: String) {
            self.name = name
            self.email = email
        }
    }

    pub resource Admin {
        pub let profile: Capability<&Profile>

        init(profile: Capability<&Profile>) {
            self.profile = profile
        }
    }

    pub fun createAdmin(profile: Capability<&Profile>): @Admin {
        return <- create Admin(profile: profile)
    }

    pub fun createProfile(name: String, email: String): @Profile {
        return <- create Profile(name: name, email: email)
    }

    init() {
    }
}