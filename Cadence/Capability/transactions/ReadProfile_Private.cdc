import Example from 0x01

transaction {

    prepare(acct: AuthAccount) {
        let profile = acct.getCapability(/private/Profile)
            .borrow<&Example.Profile>()
            ?? panic("You do not have capability!")

        log(profile.name)
        log(profile.email)
    }

}