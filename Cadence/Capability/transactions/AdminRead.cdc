import Example from 0x01

transaction {

    prepare(acct: AuthAccount) {
        let admin = acct.borrow<&Example.Admin>(from: /storage/Admin)
            ?? panic("Could not borrow the admin")

        log(admin.profile.borrow()!.name)
    }

}