import Example from 0x01

transaction {

  prepare(acct: AuthAccount) {
    acct.save(<- Example.createProfile(name: "Sungyoon", email: "sungyoon@kong"), to: /storage/Profile)

    acct.link<&Example.Profile{Example.ProfilePublic}>(/public/Profile, target: /storage/Profile)

    acct.link<&Example.Profile>(/private/Profile, target: /storage/Profile)

    let privateCapability = acct.getCapability<&Example.Profile>(/private/Profile)

    acct.save(<- Example.createAdmin(profile: privateCapability), to: /storage/Admin)
  }

  execute {
    log("Created a new Profile and linked it to the public")
  }

}