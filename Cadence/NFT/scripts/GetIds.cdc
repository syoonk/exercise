import CryptoKongs from 0x02

pub fun main(account: Address): [UInt64] {
  let collectionRef = getAccount(account).getCapability(/public/CryptoKongsCollection)
    .borrow<&CryptoKongs.Collection{CryptoKongs.CollectionPublic}>()
    ?? panic("The account doesn't have CryptoKongs collection")

  return collectionRef.getIDs()
}