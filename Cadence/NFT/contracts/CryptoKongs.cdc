import NonFungibleToken from 0x05

pub contract CryptoKongs: NonFungibleToken {

  pub var totalSupply: UInt64

  pub event ContractInitialized()
  pub event Withdraw(id: UInt64, from: Address?)
  pub event Deposit(id: UInt64, to: Address?)

  pub resource NFT: NonFungibleToken.INFT {
    pub let id: UInt64

    pub let name: String

    init() {
      self.id = CryptoKongs.totalSupply + 1

      CryptoKongs.totalSupply = CryptoKongs.totalSupply + (1 as UInt64)

      self.name = "This is Kongs NFT"
    }
  }

  pub resource interface MyCollectionPublic {
    pub fun deposit(token: @NonFungibleToken.NFT)
    pub fun getIDs(): [UInt64]
    pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT
    pub fun borrowEntireNFT(id: UInt64): &NFT
  }

  pub resource Collection:
    NonFungibleToken.Provider,
    NonFungibleToken.Receiver,
    NonFungibleToken.CollectionPublic,
    MyCollectionPublic {
    // if of the NFT --> NFT with that id
    pub var ownedNFTs: @{UInt64: NonFungibleToken.NFT}

    pub fun deposit(token: @NonFungibleToken.NFT) {
      let cryptoKongs <- token as! @NFT

      emit Deposit(id: cryptoKongs.id, to: self.owner?.address)

      self.ownedNFTs[cryptoKongs.id] <-! cryptoKongs
    }

    pub fun withdraw(withdrawID: UInt64): @NonFungibleToken.NFT {
      let token <- self.ownedNFTs.remove(key: withdrawID)
        ?? panic("This collection doesn't contain NFT with given id")

      emit Withdraw(id: withdrawID, from: self.owner?.address)

      return <- token
    }

    pub fun getIDs(): [UInt64] {
      return self.ownedNFTs.keys
    }

    pub fun borrowNFT(id: UInt64): &NonFungibleToken.NFT {
      return (&self.ownedNFTs[id] as &NonFungibleToken.NFT?)!
    }

    pub fun borrowEntireNFT(id: UInt64): &NFT {
      let refNFT = &self.ownedNFTs[id] as &NonFungibleToken.NFT?

      return refNFT as! &NFT
    }

    init() {
      self.ownedNFTs <- {}
    }

    destroy() {
      destroy self.ownedNFTs
    }
  }

  pub fun createEmptyCollection(): @Collection {
    return <- create Collection()
  }


  pub resource NFTMinter {
    pub fun createNFT(): @NFT {
      return <- create NFT()
    }

    init() {
    }
  }

  init() {
    self.totalSupply = 0 as UInt64

    emit ContractInitialized()

    self.account.save(<- create NFTMinter(), to: /storage/CryptoKongsMinter)
  }

}