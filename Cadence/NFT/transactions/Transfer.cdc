import CryptoKongs from 0x02

transaction(recipient: Address, withdrawId: UInt64) {

    prepare(acct: AuthAccount) {
        log("Signer is: ".concat(acct.address.toString()))

        let collectionRef = acct.borrow<&CryptoKongs.Collection>(from: /storage/CryptoKongsCollection)
            ?? panic("Account doesn't have CryptoKongs Collection")
        let collectionRef_recipient = getAccount(recipient).getCapability(/public/CryptoKongsCollection)
            .borrow<&CryptoKongs.Collection{CryptoKongs.MyCollectionPublic}>()
            ?? panic("Recipient doesn't have CryptoKongs Collection")

        collectionRef_recipient.deposit(token: <- collectionRef.withdraw(withdrawID: withdrawId))
    }

    execute {
        log("NFT ".concat(withdrawId.toString()).concat(" has been transferred to ").concat(recipient.toString()))
    }

}