import CryptoKongs from 0x02

transaction {

    prepare(acct: AuthAccount) {
        let collectionRef = acct.borrow<&CryptoKongs.Collection>(from: /storage/CryptoKongsCollection)
            ?? panic("Account doesn't have CryptoKongs collection")

        collectionRef.deposit(token: <- CryptoKongs.createNFT())
    }

    execute {
        log("Stored newly minted NFT into Collection")
    }

}