import CryptoKongs from 0x02
import NonFungibleToken from 0x05

transaction(recipient: Address) {

    prepare(acct: AuthAccount) {
        let nftMinter = acct.borrow<&CryptoKongs.NFTMinter>(from: /storage/CryptoKongsMinter)!

        let collectionRef = getAccount(recipient).getCapability(/public/CryptoKongsCollection)
            .borrow<&CryptoKongs.Collection{CryptoKongs.MyCollectionPublic}>()
            ?? panic("The account doesn't have CryptoKongs collection")

        collectionRef.deposit(token: <- nftMinter.createNFT())
    }

    execute {
        log("NFT has been deposited to ".concat(recipient.toString()))
    }
}