import CryptoKongs from 0x02
import NonFungibleToken from 0x05

transaction {

    prepare(acct: AuthAccount) {
        log("Signer is: ".concat(acct.address.toString()))
        log("Creating Collection...")

        acct.save(<- CryptoKongs.createEmptyCollection(), to: /storage/CryptoKongsCollection)
        acct.link<&CryptoKongs.Collection{CryptoKongs.MyCollectionPublic}>(/public/CryptoKongsCollection, target: /storage/CryptoKongsCollection)
    }

    execute {
        log("Stored a collection for CryptoKongs")
    }

}