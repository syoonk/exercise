import MyToken from 0x01
import FungibleToken from 0x02

transaction {

    prepare(acct: AuthAccount) {
        acct.save(<- MyToken.createEmptyVault(), to: /storage/Vault)
        acct.link<&MyToken.Vault{FungibleToken.Balance, FungibleToken.Receiver}>(/public/PublicVault, target: /storage/Vault)
    }

    execute {
        log("I saved my own personal vault!")
    }

}