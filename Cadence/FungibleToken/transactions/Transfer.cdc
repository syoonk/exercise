import MyToken from 0x01
import FungibleToken from 0x02

transaction(to: Address, amount: UFix64) {

    prepare(acct: AuthAccount) {
        let acctVaultRef = acct.borrow<&MyToken.Vault{FungibleToken.Provider}>(from: /storage/Vault)
            ?? panic("Cannot borrow acct vault")
        let targetVaultRef = getAccount(to).getCapability(/public/PublicVault)
            .borrow<&MyToken.Vault{FungibleToken.Receiver}>()
            ?? panic("Cannot borrow target vault")

        let sendingBalance <- acctVaultRef.withdraw(amount: amount)

        targetVaultRef.deposit(from: <- sendingBalance)
    }

    execute {
        log(amount.toString().concat(" successfully deposited to ").concat(to.toString()))
    }

}