import MyToken from 0x01
import FungibleToken from 0x02

transaction(to: Address, amount: UFix64) {

  prepare(acct: AuthAccount) {
    let minter = acct.borrow<&MyToken.Minter>(from: /storage/Minter)
      ?? panic("We could not borrow the Minter resource.")

    let newVault <- minter.mintToken(amount: amount)

    let receiverVaultRef = getAccount(to).getCapability(/public/PublicVault)
      .borrow<&MyToken.Vault{FungibleToken.Receiver}>()
      ?? panic("Cannot borrow receiver's vault")

    receiverVaultRef.deposit(from: <- newVault)
  }

  execute {
    log("Amount ".concat(amount.toString()).concat(" is minted to ").concat(to.toString()))
  }

}