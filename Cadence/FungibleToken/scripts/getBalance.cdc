import MyToken from 0x01
import FungibleToken from 0x02

pub fun main(owner: Address) {
  let vaultRef = getAccount(owner).getCapability(/public/PublicVault)
    .borrow<&MyToken.Vault{FungibleToken.Balance}>()
    ?? panic("Cannot borrow vault")

  log("balance: ".concat(vaultRef.balance.toString()))
}