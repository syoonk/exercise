import React from 'react';
import './App.css';
import {observer, useObservable} from "mobx-react-lite"; 

const App = observer(() => {
  const store = useObservable({
    count: 0,
    plusOne() {
      store.count++;
    },
    minusOne() {
      store.count--;
    }
  })

  const plusClickHandle = () => {
    store.plusOne();
  }

  const minusClickHandle = () => {
    store.minusOne();
  }

  return (
    <div className="App">
      <header className="App-header">
        <h1>Count: {store.count}</h1>
        <button onClick={plusClickHandle}>+1</button>
        <button onClick={minusClickHandle}>-1</button>
      </header>
    </div>
  );
});

export default App;
