import React, { useRef, useEffect } from "react";
import "./App.css";

function App() {
    const nameRef = useRef();
    const ageRef = useRef();
    const marriedRef = useRef();
    const submitRef = useRef();

    
    const keyPressList = {
        nameInput: () => { ageRef.current.focus(); },
        ageInput: () => { marriedRef.current.focus(); },
        marriedInput: () => { submitRef.current.focus(); },
        submitButton: () => {}
    };

    useEffect(() => {
      nameRef.current.focus();
    }, []);

    const keyPressHandle = e => {
        if(e.keyCode === 13) {
            keyPressList[e.target.id].call(this);
        }
    }

    const onClickHandle = e => {
        alert('submitted');
    }

  return (
    <div className="App">
      <header className="App-header">
        <h3>useRefs Hook</h3>
        <div className="form-field">
          <span>Name</span>
          <input ref={nameRef} id="nameInput" type="text" onKeyDown={keyPressHandle}></input>
        </div>
        <div className="form-field">
          <span>Age</span>
          <input ref={ageRef} id="ageInput" type="text" onKeyDown={keyPressHandle}></input>
        </div>
        <div className="form-field">
          <span>Married?</span>
          <input ref={marriedRef} id="marriedInput" type="checkbox" onKeyDown={keyPressHandle}></input>
        </div>
        <button onClick={onClickHandle} ref={submitRef} id="submitButton" onKeyDown={keyPressHandle}>
          Submit
        </button>
      </header>
    </div>
  );
}

export default App;
