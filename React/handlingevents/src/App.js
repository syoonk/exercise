import React, { useRef, useEffect } from "react";
import Input from "./components/Input.js";
import "./App.css";

const inputStyle = {
  width: "400px",
  height: "40px",
  fontSize: "30px",
  marginBottom: "10px"
};

function App() {
  const firstNameRef = useRef(null);
  const lastNameRef = useRef(null);

  useEffect(() => {
    firstNameRef.current.focus();
  }, []);

  const firstNameKeyDown = e => {
      if(e.key === "Enter") {
          lastNameRef.current.focus();
      }
  }

  const lastNameKeyDown = e => {
      if(e.key.code === 13) {
          
      }
  }

  return (
    <div className="App">
      <header className="App-header">
        <Input
          ref={firstNameRef}
          placeHolder="type first name here"
          style={inputStyle}
          onKeyDown={firstNameKeyDown}
        ></Input>
        <Input
          ref={lastNameRef}
          placeHolder="type last name here"
          style={inputStyle}
          onKeyDown={lastNameKeyDown}
        ></Input>
      </header>
    </div>
  );
}

export default App;