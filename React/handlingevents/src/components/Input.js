import React from "react";

function Input({ placeHolder, style }, ref, onKeyDown) {
  return (
    <input
      onKeyDown={onKeyDown}
      ref={ref}
      type="text"
      placeHolder={placeHolder}
      style={style}
    ></input>
  );
}

const ForwardedInput = React.forwardRef(Input);

export default ForwardedInput;
