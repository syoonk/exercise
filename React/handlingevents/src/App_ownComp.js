import React, { useState } from "react";
import "./App.css";
import Item from "./components/item";
import useList from "./hooks/useList";

const initList = [
  { name: "tomato", calorie: 20 },
  { name: "rice", calorie: 30 },
  { name: "candy", calorie: 100 }
];

function App() {
  const items = useList(initList);
  const [editable, setEditable] = useState(false);

  const removeHandle = e => {
    items.removeItem(e.target.name);
  };

  const editableHandle = e => {
    setEditable(true);
  }

  const keyPressHandle = (e, i) => {
    if(e.key === "Enter") {
      setEditable(!editable);
      items.saveItem(i, e.target.value);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <h2>Grocery List</h2>
        {items.list.map((v, k) => {
          return (
            <Item
              key={`${k}${v.name}${v.calorie}`}
              item={v}
              onClick={removeHandle}
              editable={editable}
              onDoubleClick={editableHandle}
              onKeyPress={keyPressHandle}
              index={k}
            ></Item>
          );
        })}
      </header>
    </div>
  );
}

export default App;
