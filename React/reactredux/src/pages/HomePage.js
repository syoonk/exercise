import React from "react";
import { useSelector, useDispatch } from "react-redux";

const HomePage = () => {
  const balance = useSelector(state => state.balanceReducer.balance);
  const loan = useSelector(state => state.loanReducer.loan);
  const dispatch = useDispatch();
  const loanClickHandle = e => {
    dispatch({ type: "APPLY" });
  };

  return (
    <div>
      <h1>balance: {balance}</h1>
      <button onClick={loanClickHandle} disabled={loan ? true : false}>
        {loan ? "Loan Applied" : "Apply for Loan"}
      </button>
    </div>
  );
};

export default HomePage;
