import React from "react";
import { useSelector, useDispatch } from "react-redux";
import * as balanceActions from "../actions/balanceActions.js";

const DepositPage = () => {
  const balance = useSelector(state => state.balanceReducer.balance);
  const loan = useSelector(state => state.loanReducer.loan);
  const loading = useSelector(state => state.balanceReducer.loading);
  const dispatch = useDispatch();
  const depositClickHandle = e => {
    dispatch(balanceActions.depositAsync());
  };

  return (
    <div>
      {loading ? <h1>saving...</h1> : <h1>balance: {balance}</h1>}
      <button onClick={depositClickHandle}>Deposit</button>
      <h1>{loan ? "Loan Applied" : "No Loan"}</h1>
    </div>
  );
};

export default DepositPage;
