const initialState = {
  loan: false
};

const loanReducer = (state = initialState, action) => {

  if(action.type === "APPLY") {
    return { loan: true };
  } else {
    return state;
  }
};

export default loanReducer;
