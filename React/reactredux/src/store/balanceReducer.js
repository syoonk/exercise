const initialState = {
  balance: 0,
  loading: false
};

const balanceReducer = (state = initialState, action) => {
  // switch(action.type) {
  //   case "DEPOSIT":
  //      return { balance: state.balance + action.payload };
  //   case "WITHDRAW":
  //      return { balance: state.balance - action.payload };
  //   default:
  //     return state;
  // }

  // It's not working either
  // const actions = {
  //   "DEPOSIT": () => {
  //     return { balance: state.balance + action.payload };
  //   },
  //   "WITHDRAW": () => {
  //     return { balance: state.balance - action.payload };
  //   },
  //   "DEFAULT": () => {
  //     return state;
  //   }
  // };

  // let invoke = actions[action.type] || actions["DEFAULT"];
  // invoke.call(this);

  if (action.type === "DEPOSIT") {
    return { balance: state.balance + action.payload, loading: false};
  } else if (action.type === "WITHDRAW") {
    return { balance: state.balance - action.payload, loading: false};
  } else if (action.type === "LOADING") {
    return { ...state, loading: true}
  } else {
    return { balance: state.balance };
  }
};

export default balanceReducer;
