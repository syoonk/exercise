import React, { useState } from "react";

function Home() {
  const submitRegisterHandle = (_e) => {
    sessionStorage.setItem('name', _e.target.elements[1].value);
    sessionStorage.setItem('phone', _e.target.elements[2].value);

    console.log("Set Name:: ", sessionStorage.getItem('name'));
    console.log("Set Phone:: ", sessionStorage.getItem('phone'));
  }

  const submitLogoutHandle = (_e) => {
    sessionStorage.clear();
  }
  
  return (
    <div>
      {
      sessionStorage.getItem('name') ? (
        <div style={{textAlign: "center"}}>
          <form onSubmit={_e => submitLogoutHandle(_e)}>
            <button type="submit" className="btn btn-primary">Logout</button>
          </form>
        </div> ) : (

        <div>
          <header>
            <h3 style={{textAlign: "center"}}>Login</h3>
          </header>

          <div className="container col-sm-4">
            <form onSubmit={_e => submitRegisterHandle(_e) }>
              <fieldset>
                <div className="form-group">
                  <label htmlFor="inputName">User Name</label>
                  <input type="name" className="form-control" id="inputName" aria-describedby="nameHelp" placeholder="Enter name"></input>
                  {/* <small id="nameHelp" className="form-text text-muted">only text</small> */}
                </div>
                <div className="form-group">
                  <label htmlFor="inputPhoneNum">Phone Number</label>
                  <input type="phoneNumber" className="form-control" id="inputPhoneNum" placeholder="Phone Number"></input>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
              </fieldset>
            </form>
          </div>
        </div> )
      }
    </div>
  )
}

export default Home;