import React, { useEffect, useState } from 'react';

const alphabets = ["a", "b", "c", "d", "e"];
const koreans = ["ㄱ", "ㄴ", "ㄷ", "ㄹ", "ㅁ"];

function MyPage() {
  const [tableContents, setTableContents] = useState([]);

  useEffect(() => {
    const createTableContents = (_alphabet, _korean) => {
      return {alphabet: _alphabet, korean: _korean};
    };
    
    let tableContentsTemp = [];
    for(let i = 0; i < alphabets.length; i++) {
      tableContentsTemp.push(createTableContents(alphabets[i], koreans[i]));
    }

    setTableContents(tableContentsTemp);

    console.log(sessionStorage.getItem('name'));
    console.log(sessionStorage.getItem('phone'));
  }, []);

  return (
    <main className="container-fluid">
      <div>
        <h1>This is my page</h1>
      </div>

      <div className="row">
        <div className="col-sm-3 first-col">
          <div className="card border-light mb-3" style={{maxWidth: "20rem"}}>
            <div className="card-header">UserInfo</div>
            <div className="card-body">
              <h4 className="card-title">User Name</h4>
              {
                sessionStorage.getItem('name') ? <p>{sessionStorage.getItem('name')}</p> : <p>no name</p>
              }
            </div>  
          </div>

          <div className="card border-light mb-3" style={{maxWidth: "20rem"}}>
            <div className="card-header">Hi</div>
            <div className="card-body">
              <h4 className="card-title">Phone</h4>
              {
                sessionStorage.getItem('phone') ? <p>{sessionStorage.getItem('phone')}</p> : <p>no phone</p>                
              }
            </div>  
          </div>

        </div>

        <div className="col-sm-8">
          <h3>Letter Table</h3>
          <table className="table table-hover">
            <thead>
              <tr>
                <th scope="col">Index</th>
                <th scope="col">Alhabet</th>
                <th scope="col">Korean</th>
              </tr>
            </thead>
            <tbody>
            {
              tableContents.map((content, idx) => (
                <tr className="table-light" key={idx}>
                  <td>{idx}</td>
                  <td>{content.alphabet}</td>
                  <td>{content.korean}</td>
                </tr>
              ))
            }
            </tbody>
          </table> 
        </div>

      </div>
    </main>
  )
}

export default MyPage;