import React, { useMemo, useState } from "react";
import Child from './components/child.js';
import "./App.css";

function App() {
  const [i, setI] = useState(0);

  const incrementClickHandle = e => {
    setI(i + 1);
  }

  const memoChild = useMemo(() => {
    return <Child></Child>
  }, [i]);  //  it would be rendered every time i changed.

  return (
    <div className="App">
      <header className="App-header">
        <h2>useMemo</h2>
        <h3>i: {i}</h3>
        <button onClick={incrementClickHandle}>Increment i</button>
        <h3>normal render</h3>
        <Child></Child>
        <h3>memo render</h3>
        {memoChild}
      </header>
    </div>
  );
}

export default App;
