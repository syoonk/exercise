// useDebugValue is added on usePrevious.js

import React, { useState } from "react";
import usePrevious from "./hooks/usePrevious.js";
import "./App.css";

function App() {
  const [age, setAge] = useState(32);
  const previousage = usePrevious(age);

  return (
    <div className="App">
      <header className="App-header">
        <h2>Use Previous</h2>
        <h3>current age: {age}</h3>
        <h3>previous age: {previousage}</h3>
        <button onClick={() => setAge(age - 1)}>Make me younger</button>
      </header>
    </div>
  );
}

export default App;
