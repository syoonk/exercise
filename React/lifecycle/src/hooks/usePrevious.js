import { useEffect, useRef, useDebugValue } from "react";

const usePrevious = value => {
  const ref = useRef(null);
  useEffect(() => {
    ref.current = value;
  });

  useDebugValue(ref.current > 19 ? "too Much" : "too Little");

  return ref.current;
};

export default usePrevious;
