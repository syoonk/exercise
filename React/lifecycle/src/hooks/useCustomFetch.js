import { useState, useEffect } from "react";

const useCustomFetch = url => {
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const customFetch = async url => {
    try {
      let response = await fetch(url);
      let jsonData = await response.json();

      setData(jsonData);
      setLoading(false);
    } catch (e) {
      setError(e);
      setLoading(false);
    }
  };

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      if (url) { // So it would not be called after cleaning up
        customFetch(url);
      }
    }, 3000);
  }, [url]);

  return [data, loading, error];
};

export default useCustomFetch;
