import React, {useContext} from "react";
import messageContext from "../../contexts/messageContext.js";

const Inner = () => {
  const [message, setMessage] = useContext(messageContext);

  return (
    <>
      <h2>Inner:</h2>
      <button onClick={() => { setMessage(Math.random().toString()) }}>Change Message</button>
    </>
  );
};

export default Inner;
