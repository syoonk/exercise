import React, {useContext} from "react";
import Inner from "./inner.js";
import messageContext from "../../contexts/messageContext.js";

const Outer = () => {
  return (
    <>
      <h2>Outer: {useContext(messageContext)[0]}</h2>
      <Inner></Inner>
    </>
  );
};

export default Outer;
