import React, { useReducer } from "react";
import "./App.css";

const initState = {
  count: 0
};

const reducerFunction = (state, action) => {
  switch (action.type) {
    case "INCREMENT":
      return { count: state.count + 1 };
    case "DECREMENT":
      return { count: state.count - 1 };
    default:
      return state;
  }
};

function App() {
  const [state, dispatch] = useReducer(reducerFunction, initState);
  
  const plusClickHandle = () => {
    dispatch({type: "INCREMENT"});
  }
  
  const minusClickHandle = () => {
    dispatch({ type: "DECREMENT" });
  };

  return (
    <div className="App">
      <header className="App-header">
        <h2>useReducer Example</h2>
        <h3>Count: {state.count}</h3>
        <button onClick={plusClickHandle}>+1</button>
        <button onClick={minusClickHandle}>-1</button>
      </header>
    </div>
  );
}

export default App;
