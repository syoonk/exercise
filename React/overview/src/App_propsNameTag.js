import React from 'react';
import './App.scss';
import NameTag from './components/nameTag';
import Input from './components/input';

const makeGreen = BaseComponent => props => {
  const addGreen = {
    style: {
      color: "green"
    }
  }

  const newProps = {
    ...props, ...addGreen
  }

  return <BaseComponent {...newProps}/>
}

const removeInline = BaseComponent => props => {
  const newProps = {...props};
  delete newProps.style;
  return <BaseComponent {...newProps}/>
}

const GreenNameTag = makeGreen(NameTag);
const CleanNameTag = removeInline(NameTag);

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="name title">Name List</h1>
          <GreenNameTag firstName="Peter" lastName="Peterson"></GreenNameTag>
          <CleanNameTag style={{color: "red"}} firstName="John" lastName="Johnson"></CleanNameTag>
          <NameTag style={{color: "red"}} firstName="Jill" lastName="Jillson"></NameTag>
          <NameTag></NameTag>

        {/* <Input placeholder="enter here" type="text"></Input> */}
      </header>
    </div>
  );
}

export default App;