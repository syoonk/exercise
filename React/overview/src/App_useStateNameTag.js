import React, {useState} from "react";
import "./App.scss";
import NameTag from "./components/nameTag";

const initialName = [
    {firstName: "Peter", lastName: "Peterson"},
    {firstName: "John", lastName: "Johnson"},
    {firstName: "Jill", lastName: "Jillson"}
];

function App() {
    const [names, setNames] = useState(initialName);

  return (
    <div className="App">
      <header className="App-header">
        <h1 className="name title">Name List</h1>
        {
            names.map((v, i) => {
                return <NameTag 
                key={`${i}${v.firstName}${v.lastName}`}
                firstName={v.firstName} 
                lastName={v.lastName}>
                </NameTag>;
            })
        }
        {/* <NameTag firstName={names[0].firstName} lastName={names[0].lastName}></NameTag>
        <NameTag firstName={names[1].firstName} lastName={names[1].lastName}></NameTag>
        <NameTag firstName={names[2].firstName} lastName={names[2].lastName}></NameTag> */}
      </header>
    </div>
  );
}

export default App;
