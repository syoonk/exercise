//const [age, setAge] = useState(21);

const React = (function(){
  let val;
  return {
    render(_Component) {
      const Comp = _Component();
      Comp.render()
    },
    useState(_init){
      val = val || _init;

      function setState(_newVal) {
        val = _newVal;
      }
      return [val, setState];
    }
  };
})();

function AgeComp() {
  const [age, setAge] = React.useStage(21);
  return {
    render() {
      console.log(age);
    },
    ageUp() {
      setAge(age + 1);
    }
  };
}

let App;

App = React.render(AgeComp);
App.ageUp();
React.render(AgeComp);
